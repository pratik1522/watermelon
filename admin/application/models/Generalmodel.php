<?php

class Generalmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function signup($email, $password) {

        $data = array('email_id' => $email, 'password' => $password);


        $this->db->select('email_id');
        $this->db->from('user');
        $array = array('email_id' => $email);

        $this->db->where($array);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 'Email already registered';
        }

        $this->db->insert('user', $data);
        $inserted_id = $this->db->insert_id();

        if ($inserted_id > 0) {
            return $inserted_id;
        } else {
            return 'Error Occurred';
        }
    }

    public function login($email, $password) {
        $this->db->select('*');
        $this->db->from('users');
        $array = array('email' => $email, 'password' => md5($password));

        $this->db->where($array);
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $userdata = array('user_id' => $row['user_id'], 'first_name' => $row['first_name'], 'last_name' => $row['last_name'], 'email_id' => $row['email_id'], 'contact_no' => $row['contact_no'], 'facebook_id' => $row['facebook_id'], 'google_id' => $row['google_id'], 'logged_in' => TRUE);

            $this->session->set_userdata($userdata);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Success! Login successfull</div>');
             return redirect('login/login');
        
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Invalid Login</div>');
            return redirect('login/login');
        }
    }

    public function addaccount() {


        //$assignee = join(',', $assignee);
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $account_type = $this->input->post('accounttype');
        $server = $this->input->post('server');
        $port = $this->input->post('port');
        $security_type = $this->input->post('securitytype');
        $data = array('user_id' => $this->session->userdata('user_id'), 'email' => $email, 'password' => $password, 'account_type' => $account_type, 'server' => $server, 'port' => $port, 'security_type' => $security_type);

        $this->db->insert('account', $data);
        $inserted_id = $this->db->insert_id();

        if ($inserted_id > 0) {
            return $inserted_id;
        } else {
            return 'Error Occurred';
        }
    }

    public function showaccount() {
        $id = $_POST['id'];
        $this->db->select("*");
        $this->db->from('accounts');
        $this->db->where('user_id', $id);
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            foreach ($query->result() AS $row) {
                $arrData[] = $row;
            }
            return json_encode($arrData);
        }
    }

    public function select_mails_for_adminpanel($table_name, $where_arr, $limit1, $limit2) {


        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($where_arr);
        $this->db->order_by('email_content_id', 'desc');

        if ($limit1 != '') {

            $this->db->limit($limit2, $limit1);
        }
        $result = $this->db->get();
        // var_dump($this->db->last_query());die;
        if ($result->num_rows() > 0) {
            //$data =(array) $result->result();
            foreach ($result->result() as $key => $value) {
                //$file = $_SERVER['DOCUMENT_ROOT'] . "/mailfiles/mails$value->email_content_id.txt";
                //$myfile = fopen($file, "w+") or die("Unable to open file!");
                //fwrite($myfile, $value->email_content);
                //$url = base_url() . "/mailfiles/mails$value->email_content_id.txt";
                $data[] = array('email_content_id' => $value->email_content_id, 'account_id' => $value->account_id, 'user_id' => $value->user_id, 'from' => $value->from, 'email_subject' => $value->email_subject, 'email_content' => $value->email_content, 'email_datetime' => $value->email_datetime, 'register_datetime' => $value->register_datetime, 'is_read' => $value->is_read, 'is_delete' => $value->is_delete, 'mail_uid' => $value->mail_uid, 'mail_sequence_no' => $value->mail_sequence_no);
                //$data[]=array('email_content_id'=>$value->email_content_id,'account_id'=>$value->account_id,'user_id'=>$value->user_id,'from'=>$value->from,'email_datetime'=>$value->email_datetime);
            }

            return $data;
        } else {
            return $result->num_rows();
        }
    }

    public function get_mails_by_category_model($table_name, $where_arr, $limit1 = '', $limit2 = '') {


//        $this->db->select('*');
//        $this->db->from($table_name);
//        $this->db->join('email_category', 'email_category.email_content_id=email_content.email_content_id', 'inner');
//        $this->db->join('category', 'category.category_id=email_category.category_id', 'inner');
//        $this->db->where($where_arr);
//        $this->db->order_by('email_content.email_content_id', 'desc');
//
//        if ($limit1 != '') {
//
//            $this->db->limit($limit2, $limit1);
//        }
        $userid = $where_arr['user_id'];
        $category_id = $where_arr['category_id'];
        // $query = "SELECT * FROM `email_content` a INNER JOIN `email_category` b ON b.`email_content_id`=a.`email_content_id` where  find_in_set('$category_id',b.category_id) and user_id=$userid order by a.email_content_id desc";
        $query = "SELECT * FROM `email_content` a INNER JOIN `email_category` b ON b.`email_content_id`=a.`email_content_id` where  b.category_id='$category_id' and user_id=$userid order by a.email_content_id desc";
        $res = $this->db->query($query);
        $result = $res->result_array();
        //   var_dump($result);die;
        if (count($result) > 0) {
            //$data =(array) $result->result();
            foreach ($result as $key => $value) {
                //$file = $_SERVER['DOCUMENT_ROOT'] . "/mailfiles/mails$value->email_content_id.txt";
                //$myfile = fopen($file, "w+") or die("Unable to open file!");
                //fwrite($myfile, $value->email_content);
                //$url = base_url() . "/mailfiles/mails$value->email_content_id.txt";
                $data[] = array('email_content_id' => $value['email_content_id'], 'account_id' => $value['account_id'], 'user_id' => $value['user_id'], 'from' => $value['from'], 'email_subject' => $value['email_subject'], 'email_content' => $value['email_content'], 'email_datetime' => $value['email_datetime'], 'register_datetime' => $value['register_datetime'], 'is_read' => $value['is_read'], 'is_delete' => $value['is_delete'], 'mail_uid' => $value['mail_uid'], 'mail_sequence_no' => $value['mail_sequence_no']);
                //$data[]=array('email_content_id'=>$value->email_content_id,'account_id'=>$value->account_id,'user_id'=>$value->user_id,'from'=>$value->from,'email_datetime'=>$value->email_datetime);
            }

            return $data;
        } else {
            return 0;
        }
    }

    public function getSingleRecord($table_name,$where){
        return $this->db->get_where($table_name,$where)->row();
    }

    public function getMultipleRecord($table_name,$where){
        return $this->db->get_where($table_name,$where)->result();
    }


}
