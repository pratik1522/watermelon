 <?php $api_url = $this->config->item('api_url');   ?>
<div class="panel">
   <div class="col-md-12" >
        <br>
     

    </div>
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li>Driver Balance List</li>
            <li>            
            <!-----file export-->
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                <ul class="dropdown-menu">
                    <li class="divider"></li>                                                
                    <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/xls.png');?>' width="24"/> XLS</a></li>
                  <!--   <li class="divider"></li>
                    <li><a href="#" onClick ="$('#example').tableExport({type:'png',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/png.png');?>' width="24"/> PNG</a></li>
                    <li><a href="#" onClick ="$('#example').tableExport({type:'pdf',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/pdf.png');?>' width="24"/> PDF</a></li> -->
                </ul>
            </div>
            <!--end-->
            </li>
        </ol>           
    </div> 

<style>
.scroll_tabb .table-responsive{overflow-x: scroll;
}
</style>

 
    <div class="panel-body">
    <div class="scroll_tabb">
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered display nowrap" id="example">
            <thead>
                <tr>
                    <th>Driver Id</th>
                    <th>Full Name </th>
                    <th>Email </th>
                    <th>Contact No </th>
                    <th>Image</th>
                    <th>Complete Orders</th>
                    <th>Total Earning </th>
                    <th>Total Driver Earning</th>
                    <th>Total Paid</th>
                    <th>Total Driver Balance </th>
                    <th>Pay Now</th>
                    <th>Pay Details</th>
                    </tr>
                    </thead>    
            <tbody>
                <?php foreach ($users as $key => $value) {
     
                    ?>
                    <tr>
                    	<td><?php echo $value->user_id; ?></td>
                        <td><?php echo $value->firstname. ' '.$value->lastname; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><?php echo $value->phone; ?></td>
                        <td><img src="<?php echo $api_url; ?>/upload/<?php echo $value->profile_image; ?>" width="50" data-toggle="modal" data-target="#myModal<?php echo $value->user_id; ?>"> </td>
                        <td><?php  echo getCount('orders',array('driver_id'=>$value->user_id,'delivery_status'=>4));?></td>
                        <td><?php  $driver_bal =getSum('orders',array('driver_id'=>$value->user_id,'delivery_status'=>4),'delivery_cost'); echo round($driver_bal,2); ?></td>
                        <td><?php  $total_driver_payment = getSum('orders',array('driver_id'=>$value->user_id,'delivery_status'=>4),'driver_delivery_cost'); echo round($total_driver_payment,2);?></td>
                        <td style="background:#b6b9b4;color:#2b861a;font-weight:600"><?php $total_paid = getSum('driver_payment',array('driver_id'=>$value->user_id),'payment'); echo $total_paid; ?></td>
                        <td style="background:#b6b9b4;color:blue;font-weight:600"><?php echo $total_balance = $total_driver_payment-$total_paid; ?></td>
                        <td><input type="button" class="btn btn-success" onClick="paymentModel<?php echo $value->user_id;?>(<?php echo $value->user_id;?>,'<?php echo $total_balance;?>')" value="Pay Now"></td>
                        <td><a href="<?php echo site_url('Admin/payment_details/'.$value->user_id);?>"><input type="button" class="btn btn-info" value="Details"></a></td>                        
                        <!-- Modal -->
                          <div class="modal fade" id="myModal<?php echo $value->user_id; ?>" role="dialog">
                            <div class="modal-dialog">    
                              <!-- Modal content-->
                              <div class="modal-content" style="text-align:center;">
                                <div class="modal-header"  style="background: rgba(0, 0, 0, 0.84);border:none;text-align:center;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>          
                                </div>
                                <div class="modal-body" style="background: rgba(0, 0, 0, 0.84);">
                                  <p><img src="<?php echo $api_url;?>upload/<?php echo $value->profile_image; ?>" width="200" style="border: 5px solid #fff;"></p>
                                </div>        
                              </div>      
                            </div>
                          </div>
                    <!--======================================model end=====================================-->  

                    <script>
                    function paymentModel<?php echo $value->user_id;?>(driver_id,payment){    
                        $('#paymentModal').modal('show');
                        $('#amount').val(payment); 
                        $('#driver_id').val(driver_id);                
                        $('#balance').val(payment);                
                        $('#bal').text(payment);                
                    }
                    </script>

                      
                        <?php } ?>
                        </tr>
        </table>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentModal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
          <center><h4 class="modal-title">Pay To Driver</h4></center>
        </div>
        <div class="modal-body"> 
            <span style="color:red"><h4>Driver Balance :<span id='bal'></span> </h4></span>  
            <input class="form-control" type="text" name="amount" id='amount' placeholder="Enter amount" required>
            <input type="hidden" id='driver_id'>
            <input type="hidden" id='balance'>
        </div> 
        <div class="modal-footer">
            <input type="button" value="Pay Now" onclick="paynowValidation()" class="btn btn-success pull-right">            
      </div>       
      </div>      
    </div>
</div>


    <script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
<!--     <script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
    <script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.3/js/dataTables.buttons.min.js"></script> -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script> 

    <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/tableExport.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jquery.base64.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/html2canvas.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/plugins/tableexport/jspdf/libs/sprintf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/jspdf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/libs/base64.js');?>"></script>  

<script>
$('#new').hide();

$(document).ready(function() {
    $('.panel').css('max-height',$(window).height() -280);
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

function paynowValidation(){
    var nowpay      = $('#amount').val();
    var driver_id   = $('#driver_id').val();
    var balance     = $('#balance').val();
    if((balance-nowpay)<0){
        var c = confirm('Pay amount is greater than driver balance. Do you want to continue');        
        if(c==true){
            var r = confirm('Are you want to pay $'+ nowpay+' to driver');
            if (r == true) {
                paynow(nowpay,driver_id);
            }        
        }
    }        
    else{        
        var r = confirm('Are you want to pay $'+ nowpay+' to driver');
        if (r == true) {
            paynow(nowpay,driver_id);
        }
    }
}

function paynow(pay,driver_id){
    if(pay!='' && driver_id!=''){
        $.ajax({
            type:'POST',
            data:{'driver_id':driver_id,'payment':pay},
            url:'<?php echo site_url("Admin/pay_to_driver"); ?>',
            dataType:'json',
            success:function(res){
                if(res.success==1){
                    alert(res.message);
                    location.reload(true);    
                }
                else{
                    alert(res.message);
                }
                console.log(res);
            },
            error:function(res){
                alert(res.message);
                location.reload(true); 
            }
        });

    }
    
}




</script>