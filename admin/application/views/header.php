<!DOCTYPE html> 
<html class=no-js>
    <!-- Mirrored from urban.nyasha.me/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Jun 2016 09:29:32 GMT -->
    <head>
        <meta charset=utf-8>
        <title>Pickanddrop Admin</title>
        <meta name=description content="">
        <meta name=viewport content="width=device-width">
        
        <link rel="shortcut icon" href="<?php echo base_url('images/pick.png');?>">
        <link rel=stylesheet href=<?php echo base_url('styles/climacons-font.249593b4.css'); ?>>        
        <link rel=stylesheet href=<?= base_url('vendor/datatables/media/css/jquery.dataTables.css'); ?>>
		<link rel=stylesheet href=<?= base_url('vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css'); ?>>
		<link rel=stylesheet href="https://cdn.datatables.net/buttons/1.2.3/css/buttons.dataTables.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="<?= base_url('styles/app.min.df5e9cc9.css'); ?>">
		<link rel="stylesheet" href="<?= base_url('styles/custom.css'); ?>">
		</head>
    <body>
        <div class="app layout-fixed-header">
            <div class="sidebar-panel offscreen-left">
                <div class=brand>
                    <div class=brand-logo> <img src="<?php echo base_url('images/pick.png');?>" style="width:170px;height:280px;margin-top:-131px" alt="logo Image"> </div>
                    <a href=javascript:; class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle=layout-small-menu> <span></span> <span></span> <span></span> <span></span> </a>   
                </div>
                <nav role=navigation>
                    <ul class=nav style="margin-top: 40px;">
                        <!--<li> <a href=<?php echo site_url('admin/dashboard'); ?>> <i class="fa fa-flask"></i> <span>Dashboard</span> </a> </li>-->
                        
                        <li>
                            <a href=<?php echo site_url('admin/userlist'); ?>> <i class="fa fa-user"></i> <span>Customers</span> </a> 
<!--                            <ul class=sub-menu>
                                <li> <a href=<?php echo site_url('admin/createuser'); ?> <span>Create User</span> </a> </li>
                                <li> <a href=<?php echo site_url('admin/userlist'); ?> <span>View User</span> </a> </li>
                            </ul>-->
                        </li>
                        <li>
                            <a href=<?php echo site_url('admin/driverlist'); ?>> <i class="fa fa-user"></i> <span>Drivers</span> </a> 

                        </li>
                        <!--<li>
                            <a href=<?php echo site_url('admin/feedbacklist'); ?>> <i class="fa fa-user"></i> <span>Feedback</span> </a> 

                        </li>-->
                        <li>
                            <a href=<?php echo site_url('admin/driver_payment'); ?>> <i class="fa fa-user"></i> <span>Driver Payment</span> </a> 

                        </li>
                         <li> <a href=<?php echo site_url('admin/orderlist'); ?>> <i class="fa fa-toggle-on"></i><span>Orders</span> </a> </li>

                         <li> <a href=<?php echo site_url('admin/savesettings'); ?>> <i class="fa fa-toggle-on"></i><span>Settings</span> </a> </li>
                       <li> <a href=<?php echo site_url('admin/driverlocation'); ?>> <i class="fa fa-toggle-on"></i><span>Driver Location</span> </a> </li>
                      <!--  <li> <a href=<?php echo site_url('admin/deliverylocation'); ?>> <i class="fa fa-toggle-on"></i><span>Delivery Location</span> </a> </li>  -->
                         <?php 
                         $user_data=$this->session->all_userdata();
                         if($user_data['admin_type']=='super'){ ?>
                         <li> <a href=<?php echo site_url('admin/adminlist'); ?>> <i class="fa fa-toggle-on"></i><span>Admin User</span> </a> </li>
                         <?php } ?>
                        
                    </ul>
                </nav>
            </div>
            <div class=main-panel>
                <header class="header navbar">
                    <div class="brand visible-xs">
                        <div class=toggle-offscreen> <a href=# class="hamburger-icon visible-xs" data-toggle=offscreen data-move=ltr> <span></span> <span></span> <span></span> </a> </div>
                        <div class=brand-logo> <img src=<?php echo base_url('images/logo-dark.5ba260bb.png'); ?> height=15 alt=""> </div>
                        <div class=toggle-chat> <a href=javascript:; class="hamburger-icon v2 visible-xs" data-toggle=layout-chat-open> <span></span> <span></span> <span></span> </a> </div>
                    </div>
                    <ul class="nav navbar-nav hidden-xs">
                        <li>
                            <p class=navbar-text>  </p>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right hidden-xs">
                        
                        <li>
                        </li>
                        <li>
                            <a href=javascript:; data-toggle=dropdown> <img src=<?php echo base_url('images/avatar.21d1cc35.jpg'); ?> class="header-avatar img-circle ml10" alt=user title=user> <span class=pull-left><?php echo $this->session->userdata('name'); ?></span> </a> 
                            <ul class=dropdown-menu>
                               <!-- <li> <a href=javascript:;>Settings</a> </li>-->
                                <!--<li> <a href=javascript:;> <span class="label bg-danger pull-right">34</span> <span>Notifications</span> </a> </li>-->
                               
                                <li> <a href=<?= site_url('Admin/logout'); ?>>Logout</a> </li>
                            </ul>
                        </li>
                        <!--<li> <a href=javascript:; class="hamburger-icon v2" data-toggle=layout-chat-open> <span></span> <span></span> <span></span> </a> </li>-->
                    </ul>
                </header>
                <div class=main-content>