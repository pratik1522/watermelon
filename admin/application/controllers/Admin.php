<?php

if(!defined('BASEPATH'))exit('No direct script access allowed');
ob_start();

class Admin extends CI_Controller {

    function __construct() {

        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('Authentication');
        $this->load->library('push');
        $this->load->library('firebase');
        /*if($this->session->userdata('user_id')=='')
        {
            redirect(base_url('Auth'));
        }*/
        /*if($this->session->userdata('email') == ''){
            redirect('Adminin');
        }*/
    }

    public function index() {
              // echo "fdfs";die;
             /*if ($this->session->userdata('logged_in')) {
                    redirect('Admin/userlist');
                }*/
        if (!empty($_POST)) {
            $email = $this->input->post('email');
             $pass = $this->input->post('password');
             //print_r($email);print_r($pass);die;
             $where_arr = array('email'=>$email,'password'=>$pass);
            
            //$res = $this->Authentication->select_rowwhere_query('user',$where_arr);
            $res = $this->Authentication->select_rowwhere_query('user',$where_arr);
            // print_r($res);die;
            if($res > 0){
               $userdata = array('user_id' => $res->user_id,'name' => $res->firstname.' '.$res->lastname, 'email' => $res->email,'admin_type' => $res->admin_type, 'logged_in' => TRUE);
            $user_id = $res->user_id;
            $this->session->set_userdata($userdata);
               //print_r($this->session->userdata('user_id'));die;

                /*$this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              success! Login Successfull</div>');*/       
            
                 redirect('Admin/userlist');
                } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Error! Invalid Credentials</div>');
                $this->load->view('login');
            }
        } else {

            $this->load->view('login');
        }
        /*$this->load->view('header');
        $this->load->view('dashboard');
        $this->load->view('footer');*/
    }
     public function userlist() {
       //echo $this->session->userdata('email');
      if($this->session->userdata('email')!=''){ 
        //if($this->session->userdata('email') != ''){
      // echo "fd";die;
        $this->load->view('header');
        $data = $this->Authentication->select_data('user', array('type' => 0, 'user_type' => 1));
        $data = array('users' => $data);
        $this->load->view('userlist', $data);
       }else{
        redirect('Admin');
       }
//        $this->load->view('footer');
    //}
}

    public function dashboard() {
        $this->load->view('header');
        //$this->load->view('dashboard');
        //$this->load->view('footer');
    }

    public function orderlist() {
    if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_orders();
        $data = array('orders' => $data);
        $this->load->view('orderlist', $data);
//      $this->load->view('footer');
    }else{
        redirect('Admin');
    }
}

    public function filterorders() {
        $this->load->view('header');

        $data = $this->Authentication->filter_orders();
         return $data;
//        $data = array('orders' => $data);
//        $this->load->view('orderlist', $data);
//        $this->load->view('footer');
    }

    public function deliverylist() {
        if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_orders();
        $data = array('orders' => $data);
        $this->load->view('deliverylist', $data);
    }else{
        redirect('Admin');
    }
//      $this->load->view('footer');
    }

    public function managelocation() {
        $this->load->view('header');

        $data = $this->Authentication->select_orders();

        $data = array('users' => $data);
        $this->load->view('managelocation', $data);
        $this->load->view('footer');
    }

    public function search() {

        $name = $this->input->post('name');
        $data = $this->Authentication->search($name);
        if (count($data) > 0) {
            echo json_encode($data);
        }
    }

   

    public function adminlist() {
        $this->load->view('header');
        $data = $this->Authentication->select_data('user', array('type' => 0, 'type' => 1, 'admin_type' => 'normal'));
        $data = array('users' => $data);
        $this->load->view('adminlist', $data);
//        $this->load->view('footer');
    }

    public function createadmin() {
        $data_user = $this->Authentication->select_data('user');
        $data = array('data_user' => $data_user);
        $this->load->view('header');
        $this->load->view('createadmin');
        $this->load->view('footer');
    }
    public function driverlocation() {
        if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_driver_location();
       //echo "<pre>";print_r($data);die;
        $vtype = $this->Authentication->select_driver_vtype();   
        $data = array('mappoint' => $data,'vtype' =>$vtype);      
        //$data = array('vtype' => $vtype);
      //	echo '<pre>';print_r($vtype);die;      
       $this->load->view('driverlocation', $data);
   }else{

       redirect('Admin');
   }
//       $this->load->view('footer');
    }
    public function deliverylocation() {
      if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_delivery_location();
        $vtype = $this->Authentication->select_driver_vtype(); 
        $data = array('mappoint' => $data,'vtype' =>$vtype);         
      	 //echo '<pre>';print_r($vtype);die;   
        //print_r($data);die;      
        $this->load->view('deliverylocation', $data);
//       $this->load->view('footer');
    }else{
         redirect('Admin');
    }
    }
    public function createnewadmin() {
        $post = $this->input->post();


        $insertearr = array('firstname' => $this->input->post('firstname'), 'lastname' => $this->input->post('lastname'), 'email' => $this->input->post('email'), 'password' => $this->input->post('password'), 'type' => 1,'user_status' => 1);
//        $insertearr = array('title' => $this->input->post('title'),'image' => $enc_file_name);
         $id = $this->Authentication->insert('user', $insertearr);

        if ($id) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Admin user has been created successfully</div>');
            return redirect('admin/adminlist');
        }
    }

    public function editadmin($id) {
        $data = $this->Authentication->select_data('user', array('user_id' => $id));
        $data = array('userdata' => $data);
        $this->load->view('header');
        $this->load->view('editadmin', $data);
//        $this->load->view('footer');
    }

    public function updateadmin($id) {
        $post = $this->input->post();

        $updatearr = array('firstname' => $this->input->post('firstname'), 'lastname' => $this->input->post('lastname'), 'email' => $this->input->post('email'), 'password' => $this->input->post('password'), 'type' => 1);

        $id = $this->Authentication->update('user', array('user_id' => $id), $updatearr);

        if ($id) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User has been updated successfully</div>');
            return redirect('admin/adminlist');
        }
    }

    public function driverlist() {
        if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_data('user', array('type' => 0, 'user_type' => 2));
        $data = array('users' => $data);
        $this->load->view('driverlist', $data);
//        $this->load->view('footer');
    }else{
        redirect('Admin');
    }
}

public function feedbacklist() {
        if($this->session->userdata('email')!=''){
        $this->load->view('header');
        $data = $this->Authentication->select_data('driver_report', false);
        $data = array('users' => $data);
        $this->load->view('feedback', $data);
//        $this->load->view('footer');
    }else{
        redirect('Admin');
    }
}

    public function driver_payment(){ 
    if($this->session->userdata('email')!=''){       
        $this->load->view('header');
        $data = $this->Authentication->select_data('user', array('type' => 0, 'user_type' => 2));
        $data = array('users' => $data);
        $this->load->view('driver_payment', $data);
    }else{
        redirect('Admin');
    }

}

    public function pay_to_driver(){    //ajax use
        if(isset($_POST['driver_id']) && $_POST['driver_id']!=''){
            extract($_POST);
            $data = array('driver_id'=>$driver_id,'payment'=>$payment);
            $res = $this->Authentication->insert('driver_payment', $data);
            if($res>0){
                $response = array('success'=>1,'message'=>'Record has been successfully saved');
                echo json_encode($response);
            }
            else{
                $response = array('success'=>0,'message'=>'Sorry! Something went wrong. Please try again');
                echo json_encode($response);   
            }
        }
        else{
            $response = array('success'=>0,'message'=>'Sorry! Required filed is not filled. Contact with technical support');
            echo json_encode($response);   
        }

    }
     public function payment_details($driver_id){        
        if($driver_id!=''){
            $this->load->view('header');
            $data = $this->Authentication->select_data('driver_payment', array('driver_id'=>$driver_id));
            $data = array('users' => $data);
            $this->load->view('driver_payment_list', $data);
        }
        else{
            redirect('Admin/driver_payment');
        }        
    }

    public function createorder() {
        $data_user = $this->Authentication->select_data('user');
        $data = array('data_user' => $data_user);
        $this->load->view('header');
        $this->load->view('createorder', $data);
        $this->load->view('footer');
    }

    public function createproduct() {
        $this->load->view('header');
        $this->load->view('createproduct');
    }

    public function createnewproduct() {
        $post = $this->input->post();

        $picture = $_FILES['image']['name'];
        $path = $_FILES['image']['tmp_name'];

        if (!empty($picture)) {
            $enc_file_name = time() . $picture;
        } else {
            $enc_file_name = "default.jpg";
        }

        if (!empty($picture)) {
            move_uploaded_file($path, 'upload/' . $enc_file_name);
        }
        $insertearr = array('title' => $this->input->post('title'), 'image' => $enc_file_name, 'price' => $this->input->post('price'), 'colour' => $this->input->post('colour'), 'length'=>$this->input->post('length'),'height'=>$this->input->post('height'),'width'=>$this->input->post('width'),'weight'=>$this->input->post('weight'),'seller_name'=>$this->input->post('seller'),'description' => $this->input->post('descriptions'));
//        $insertearr = array('title' => $this->input->post('title'),'image' => $enc_file_name);
        $productid = $this->Authentication->insert('products', $insertearr);

        if ($productid) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Product has been created successfully</div>');
            return redirect('admin/productslist');
        }
    }

    public function updateproduct($id) {
        $post = $this->input->post();
        if (!empty($_FILES)) {
            $picture = $_FILES['image']['name'];
            $path = $_FILES['image']['tmp_name'];

            if (!empty($picture)) {
                $enc_file_name = time() . $picture;
            } else {
                $enc_file_name = "default.jpg";
            }

            if (!empty($picture)) {
                move_uploaded_file($path, 'upload/' . $enc_file_name);
            }
            $updatearr = array('title' => $this->input->post('title'), 'image' => $enc_file_name, 'price' => $this->input->post('price'), 'colour' => $this->input->post('colour'),'length'=>$this->input->post('length'),'height'=>$this->input->post('height'),'width'=>$this->input->post('width'),'weight'=>$this->input->post('weight'),'seller_name'=>$this->input->post('seller'), 'description' => $this->input->post('descriptions'));
        } else {
            $updatearr = array('title' => $this->input->post('title'), 'price' => $this->input->post('price'), 'colour' => $this->input->post('colour'),'length'=>$this->input->post('length'),'height'=>$this->input->post('height'),'width'=>$this->input->post('width'),'weight'=>$this->input->post('weight'),'seller_name'=>$this->input->post('seller'), 'description' => $this->input->post('descriptions'));
        }
        $productid = $this->Authentication->update('products', array('id' => $id), $updatearr);

        if ($productid) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Product has been updated successfully</div>');
            return redirect('admin/productslist');
        }
    }

    public function editproduct($id) {
        $data = $this->Authentication->select_data('products', array('id' => $id));
        $data = array('product' => $data);
        $this->load->view('header');
        $this->load->view('editproduct', $data);
//        $this->load->view('footer');
    }

    public function recipientdetails($id) {
        $data = $this->Authentication->select_data('orders', array('order_id' => $id));
        $api_url = $this->config->item('api_url');
        foreach ($data as $key => $value) {
            $data[$key]->signature_image = $api_url."upload/" . $value->signature_image;
            $data[$key]->received_user_image = $api_url."upload/" . $value->received_user_image;
            $data[$key]->signature_name = $value->signature_name ;
        }
        $data = array('recipientdetails' => $data);
        $this->load->view('header');
        $this->load->view('recipientdetails', $data);
//        $this->load->view('footer');
    }

    public function productslist() {
        $this->load->view('header');
        $data = $this->Authentication->select_products();
        foreach ($data as $key => $value) {
            $data[$key]->image = "../upload/" . $value->image;
        }
        $data = array('products' => $data);
        $this->load->view('productslist', $data);
//        $this->load->view('footer');
    }

    public function orderproducts($id) {
        $api_url = $this->config->item('api_url');
        $this->load->view('header');
        $data1 = $this->Authentication->select_order_shop_details($id);
        $data = $this->Authentication->select_order_products($id);
        foreach ($data as $key => $value) {
            $data[$key]->image = base_url()."upload/" . $value->image;
        }
        $data = array('products' => $data,'shop' => $data1);      
        $this->load->view('orderproducts', $data);
//        $this->load->view('footer');
    }

    public function logout() {


        $this->session->sess_destroy();

        return redirect('Admin');
    }

    public function createuser() {


        $this->load->view('header');
        $this->load->view('createuser');
//        $this->load->view('footer');
    }

    public function editorder($id) {
        $ordArr = [];
        $data = $this->Authentication->select_data('orders', array('order_id' => $id));
        $ordProd = $this->Authentication->select_data('order_products', array('order_id' => $id));
        $allProd = $this->Authentication->select_data('products', array('status' => 'Active'));
        foreach ($ordProd as $value) {
            array_push($ordArr, $value->product_id);
        }
//       echo '<pre>';
//       print_r($allProd); die;
        $data = array('orders' => $data, 'allProd' => $allProd, 'ordArr' => $ordArr);
        $this->load->view('header');
        $this->load->view('editorder', $data);
//        $this->load->view('footer');
    }

    public function createorder_code() {
        $post = $this->input->post();

        $insertearr = array('title' => $post['title'], 'weight' => $post['weight'], 'price' => $post['price'], 'receivers_address' => $post['receivers_address'], 'pincode' => $post['pincode']);
        $orderid = $this->Authentication->insert('orders', $insertearr);
        if ($orderid) {

            $arr = array('user_id' => $post['user_delivery_id'], 'status' => $post['user_delivery_status'], 'order_id' => $orderid);
            $this->Authentication->insert('order_delivery_status', $arr);
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Order has been created successfully</div>');
            return redirect('admin/orderlist');
        }
    }

    public function createuser_code() {
        $post = $this->input->post();


        $orderid = $this->Authentication->insert('user', $post);
        if ($orderid) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Order has been created successfully</div>');
            return redirect('admin/userlist');
        }
    }

    public function deleteuser($id) {
        $post = $this->input->post();

        $res = $this->Authentication->delete('user', array('user_id' => $id));
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User Deleted successfully</div>');
            return redirect('admin/userlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/userlist');
        }
    }

      public function deleteadmin($id) {
        $post = $this->input->post();

        $res = $this->Authentication->delete('user', array('user_id' => $id));
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User Deleted successfully</div>');
            return redirect('admin/adminlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/adminlist');
        }
    }

public function deletedriver($id) {
        $post = $this->input->post();

        $res = $this->Authentication->delete('user', array('user_id' => $id));
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User Deleted successfully</div>');
            return redirect('admin/driverlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/driverlist');
        }
    }

    public function edituser($id) {


        $res = $this->Authentication->select_data('user', array('user_id' => $id));
        $data = array('userdata' => $res);
        $this->load->view('header');
        $this->load->view('edituser', $data);
//        $this->load->view('footer');
    }

    public function editdriver($id) {


        $res = $this->Authentication->select_data('user', array('user_id' => $id));
        $data = array('userdata' => $res);
        $this->load->view('header');
        $this->load->view('editdriver', $data);
//        $this->load->view('footer');
    }

    public function savesettings() {

if($this->session->userdata('email')!=''){
        $data = $this->Authentication->select_data('setting');
        $data['m'] = $this->Authentication->select_minimum();
        $data['parcel'] = $this->Authentication->select_standard();
        $data['setting'] = $this->Authentication->select_setting();
         //echo '<pre>';print_r($data['parcel']);die;
        //$data = array('data' => $data);

        $this->load->view('header');
        $this->load->view('editsetting', $data);
        $this->load->view('footer');
    }else{

        redirect('Admin');
    }
}
    public function updatesetting() {
        $height = $this->input->post('height');
        $width = $this->input->post('width');
        $weight = $this->input->post('weight');
        $length = $this->input->post('length');
        $distance = $this->input->post('distance');
        $bike = $this->input->post('bike');
        $car = $this->input->post('car');
        $truck = $this->input->post('truck');
        $van = $this->input->post('van');

        $arr = array('height' => $height, 'width' => $width, 'weight' => $weight, 'length' => $length, 'distance' => $distance, 'bike' => $bike, 'car' => $car, 'truck' => $truck, 'van' => $van);
        $selres = $this->Authentication->select_data('setting');
                //echo '<pre>';
                //print_r($selres); die;
        if ($selres) {
            $res = $this->Authentication->update('setting', array(), $arr);
        } else {
            $post = $this->input->post();
            $res = $this->Authentication->insert('setting', $post);
        }
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }    
    public function updateminimum() {
       //editdriver_code echo "fdgdf";die;
        $motorbike= $this->input->post('motorbike');
        $car= $this->input->post('car');
        $van= $this->input->post('van');
        $truck= $this->input->post('truck');
        $id= $this->input->post('minimum_id');        

        $arr = array('motorbike' => $motorbike, 'car' => $car, 'van' => $van, 'truck' => $truck,'id' =>$id);
        //print_r($arr);die;
        $res=$this->Authentication->update_minimumcharges($arr,$id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    public function updateparcel() {
        $height= $this->input->post('height');
        $width= $this->input->post('width');
        $length= $this->input->post('length');
        $weight= $this->input->post('weight');
        $cost= $this->input->post('cost');
        $parcel_id= $this->input->post('parcel_id');     

        $arr = array('height' => $height, 'width' => $width, 'length' => $length, 'weight' => $weight, 'cost' => $cost ,'id' =>$parcel_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_standardparcel($arr,$parcel_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    public function updatemotorsetting() {
        $m_2hour= $this->input->post('m_2hour');
        $m_4hour= $this->input->post('m_4hour');
        $m_sameday= $this->input->post('m_sameday');        
        $m_id= $this->input->post('m_id');

        $arr = array('m_2hour' => $m_2hour, 'm_4hour' => $m_4hour, 'm_sameday' => $m_sameday, 'id' =>$m_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_motorbike($arr,$m_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    public function updatecarsetting() {
        $c_2hour= $this->input->post('c_2hour');
        $c_4hour= $this->input->post('c_4hour');
        $c_sameday= $this->input->post('c_sameday');        
        $c_id= $this->input->post('c_id');

        $arr = array('c_2hour' => $c_2hour, 'c_4hour' => $c_4hour, 'c_sameday' => $c_sameday, 'id' =>$c_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_car($arr,$c_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    public function updatevansetting() {
       // echo "dfsf";die;
        $v_2hour= $this->input->post('v_2hour');
        $v_4hour= $this->input->post('v_4hour');
        $v_sameday= $this->input->post('v_sameday');        
        $v_id= $this->input->post('v_id');

        $arr = array('v_2hour' => $v_2hour, 'v_4hour' => $v_4hour, 'v_sameday' => $v_sameday, 'id' =>$v_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_van($arr,$v_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    public function updatetrucksetting() {
        $t_2hour= $this->input->post('t_2hour');
        $t_4hour= $this->input->post('t_4hour');
        $t_sameday= $this->input->post('t_sameday');        
        $t_id= $this->input->post('t_id');

        $arr = array('t_2hour' => $t_2hour, 't_4hour' => $t_4hour, 't_sameday' => $t_sameday, 'id' =>$t_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_truck($arr,$t_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
    public function updateendtimesetting() {
        $endtime_4hour= $this->input->post('endtime_4hour');
        $endtime_sameday= $this->input->post('endtime_sameday');
        $endtime_id= $this->input->post('endtime_id');        

        $arr = array('endtime_4hour' => $endtime_4hour, 'endtime_sameday' => $endtime_sameday, 'id' =>$endtime_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_endtime($arr,$endtime_id);       
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
    public function updatedriversetting() {
        $driver_percentage= $this->input->post('driver_percentage');       
        $percentage_id= $this->input->post('percentage_id');

        $arr = array('driver_percentage' => $driver_percentage, 'id' =>$percentage_id);
        //print_r($arr);die;
        $res=$this->Authentication->update_driverpercentage($arr,$percentage_id);        
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
    
    public function updatemparcelsizesetting() {
        $m_p_height= $this->input->post('m_p_height');
        $m_p_length= $this->input->post('m_p_length');
        $m_p_width= $this->input->post('m_p_width');
        $m_p_weight= $this->input->post('m_p_weight');        
        $m_p_id= $this->input->post('m_p_id');     

        $arr = array('m_p_height' => $m_p_height, 'm_p_length' => $m_p_length, 'm_p_width' => $m_p_width, 'm_p_weight' => $m_p_weight, 'id' =>$m_p_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_mparcelsize($arr,$m_p_id);
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
     public function updatecparcelsizesetting() {
        $c_p_height= $this->input->post('c_p_height');
        $c_p_length= $this->input->post('c_p_length');
        $c_p_width= $this->input->post('c_p_width');
        $c_p_weight= $this->input->post('c_p_weight');        
        $c_p_id= $this->input->post('c_p_id');     

        $arr = array('c_p_height' => $c_p_height, 'c_p_length' => $c_p_length, 'c_p_width' => $c_p_width, 'c_p_weight' => $c_p_weight, 'id' =>$c_p_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_cparcelsize($arr,$c_p_id);
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
     public function updatevparcelsizesetting() {
        $v_p_height= $this->input->post('v_p_height');
        $v_p_length= $this->input->post('v_p_length');
        $v_p_width= $this->input->post('v_p_width');
        $v_p_weight= $this->input->post('v_p_weight');        
        $v_p_id= $this->input->post('v_p_id');     

        $arr = array('v_p_height' => $v_p_height, 'v_p_length' => $v_p_length, 'v_p_width' => $v_p_width, 'v_p_weight' => $v_p_weight, 'id' =>$v_p_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_vparcelsize($arr,$v_p_id);
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
     public function updatetparcelsizesetting() {
        $t_p_height= $this->input->post('t_p_height');
        $t_p_length= $this->input->post('t_p_length');
        $t_p_width= $this->input->post('t_p_width');
        $t_p_weight= $this->input->post('t_p_weight');        
        $t_p_id= $this->input->post('t_p_id');     

        $arr = array('t_p_height' => $t_p_height, 't_p_length' => $t_p_length, 't_p_width' => $t_p_width, 't_p_weight' => $t_p_weight, 'id' =>$t_p_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_mparcelsize($arr,$t_p_id);
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }
    
    public function updatepdmin() {
        $p_d_min_charge= $this->input->post('p_d_min_charge');               
        $p_d_min_id= $this->input->post('p_d_min_id');     

        $arr = array('p_d_min_charge' => $p_d_min_charge,'id' =>$p_d_min_id);
       // print_r($arr);die;
        $res=$this->Authentication->update_pdmin($arr,$p_d_min_id);
        if ($res==1) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Settings saved successfully</div>');
            return redirect('admin/savesettings');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/savesettings');
        }
    }


    public function updateorder($id) {
        
        $pickup_house_number = $this->input->post('pickup_house_number');
        $pickup_street_name = $this->input->post('pickup_street_name');
        $pickup_street_suff = $this->input->post('pickup_street_suff');
        $pickup_suburb = $this->input->post('pickup_suburb');
        $pickup_state = $this->input->post('pickup_state');
        $pickup_postcode = $this->input->post('pickup_postcode');
        $pickup_country = $this->input->post('pickup_country');
        $dropoff_house_number = $this->input->post('dropoff_house_number');
        $dropoff_stree_name = $this->input->post('dropoff_stree_name');
        $dropoff_street_suff = $this->input->post('dropoff_street_suff');
        $dropoff_suburb = $this->input->post('dropoff_suburb');
        $dropoff_state = $this->input->post('dropoff_state');
        $dropoff_postcode = $this->input->post('dropoff_postcode');
        $dropoff_country = $this->input->post('dropoff_country');

        $arr = array('pickup_house_number' => $pickup_house_number, 'pickup_street_name' => $pickup_street_name, 'pickup_street_suff' => $pickup_street_suff, 'pickup_suburb' => $pickup_suburb, 'pickup_state' => $pickup_state, 'pickup_postcode' => $pickup_postcode, 'pickup_country' => $pickup_country, 'dropoff_house_number' => $dropoff_house_number, 'dropoff_stree_name' => $dropoff_stree_name, 'dropoff_street_suff' => $dropoff_street_suff, 'dropoff_suburb' => $dropoff_suburb, 'dropoff_state' => $dropoff_state, 'dropoff_postcode' => $dropoff_postcode, 'dropoff_country' => $dropoff_country);
        $res = $this->Authentication->update('orders', array('order_id' => $id), $arr);
        $this->db->delete('order_products', array('order_id' => $id));
        foreach ($this->input->post('order_product') as $Prod) {
            $Prods = $this->Authentication->select_data('products', array('id' => $Prod));
            $title = $Prods[0]->title;
            $image = $Prods[0]->image;
            $price = $Prods[0]->price;
            $colour = $Prods[0]->colour;
            $description = $Prods[0]->description;

            $insertearr = array('order_id' => $id, 'product_id' => $Prods[0]->id, 'title' => $title, 'image' => $image, 'price' => $price, 'colour' => $colour, 'description' => $description);
//        $insertearr = array('title' => $this->input->post('title'),'image' => $enc_file_name);
            $productid = $this->Authentication->insert('order_products', $insertearr);
        }


        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Order Updated successfully</div>');
            return redirect('admin/orderlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/orderlist');
        }
    }

    public function assignvehicle($orderid) {


//        $res = $this->Authentication->select_data('user', array('user_id' => $id));
//        $data = array('userdata' => $res);
        $this->load->view('header');
        $this->load->view('assignvehicle');
//        $this->load->view('footer');
    }

    public function assigndriver($orderid) {


        $res = $this->Authentication->select_data('user', array('user_type' => 2));
        $data = array('drivers' => $res);
        $this->load->view('header');
        $this->load->view('assigndriver', $data);
//        $this->load->view('footer');
    }

    public function updatevehicle($orderid) {
        
        $ordDetails = $this->db->query("select * from orders where order_id='$orderid'")->row(); 
        $prvCost=strtolower($ordDetails->delivery_cost);
        $prvVehicle=strtolower($ordDetails->vehicle_type);
        $delivery_cost=$ordDetails->delivery_cost;
        
        $type = $this->input->post('vehicle');
        $vehicle_type = strtolower($type);

        //$settingResult = $this->db->query("select ".$vehicle_type.",".$prvVehicle." from setting")->row(); 

//        $currentAmt= $settingResult->$vehicle_type;
//        $previousAmt= $settingResult->$prvVehicle;
//        
//        $delivery_cost = $delivery_cost - $previousAmt;
//        $delivery_cost = $delivery_cost + $currentAmt;
        
//        $remaining = $delivery_cost - $prvCost;
        $res = $this->Authentication->update('orders', array('order_id' => $orderid), array('vehicle_type' => $type));
        
        
        $cusResult = $this->db->query("select o.*,u.device_token,u.device_type from orders o left join user u on o.user_id=u.user_id where o.order_id='$orderid'")->row();

        $token = $cusResult->device_token;
        $toId = $cusResult->user_id;
        $deviceType = $cusResult->device_type;
        
        
//        echo "select ".$vehicle_type." from setting";
        
        /*if ($deviceType == 'android') {
            // optional payload
            $payload = array();
            $payload['team'] = '';
            $payload['score'] = '';

            // notification title
            $title = 'New Vehicle';

            // notification message
            $message = 'Admin assigned your delivery a new vehicle.You need to pay $'.$remaining.' more.';

            // push type - single user / topic
            $push_type = 'individual';

            // whether to include to image or not
            $include_image = FALSE;


            $this->push->setTitle($title);
            $this->push->setMessage($message);
            if ($include_image) {
//                    $push->setImage('http://api.androidhive.info/images/minion.jpg');
            } else {
                $this->push->setImage('');
            }
            $this->push->setIsBackground(FALSE);
            $this->push->setPayload($payload);


            $json = '';
            $response = '';

            if ($push_type == 'topic') {
                $json = $this->push->getPush();
                $response = $this->firebase->sendToTopic('global', $json);
            } else if ($push_type == 'individual') {
                $json = $this->push->getPush();
                $regId = $token;
                $response = $this->firebase->send($regId, $json);
                
            }
            $jsonArray = json_decode($response,true);
            if($jsonArray['failure']){
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Vehicle assigned successfully</div>');
            return redirect('admin/orderlist');
            }
           
        } else {

            $deviceToken = $token;
            $ctx = stream_context_create();
            // ck.pem is your certificate file
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'devAPNsCertificates.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', '123');
            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            // Create the payload body
            $body['aps'] = array(
                'alert' => array(
                    'title' => 'New Vehicle',
                    'body' => 'Admin assigned your delivery a new vehicle.You need to pay $'.$remaining.' more.',
                ),
                'sound' => 'default'
            );
            // Encode the payload as JSON
            $payload = json_encode($body);
            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            // Close the connection to the server
            fclose($fp);
            
            $jsonArray = json_decode($result,true);
            if($jsonArray['failure']){
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Vehicle assigned successfully</div>');
            return redirect('admin/orderlist');
            }
        }*/
        
        $insertearr = array('order_id' => $orderid, 'to_user_id' => $toId, 'message' => 'Admin assigned your delivery a new vehicle.', 'type' => 'admin');
        $inres = $this->Authentication->insert('notifications', $insertearr);
        
        

        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Vehicle assigned successfully</div>');
            return redirect('admin/orderlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/orderlist');
        }
    }

    public function updatedriver($orderid) {
        $driver = $this->input->post('driver');
        $res = $this->Authentication->update('orders', array('order_id' => $orderid), array('driver_id' => $driver,'delivery_status' => 6));


        $driverResult = $this->db->query("select * from user where user_id='$driver'")->row();

        $token = $driverResult->device_token;
        $toId = $driverResult->user_id;
        $deviceType = $driverResult->device_type;
/*
        if ($deviceType == 'android') {
            // optional payload
            $payload = array();
            $payload['team'] = '';
            $payload['score'] = '';

            // notification title
            $title = 'New Delivery';

            // notification message
            $message = 'Admin assigned you a delivery';

            // push type - single user / topic
            $push_type = 'individual';

            // whether to include to image or not
            $include_image = FALSE;


            $this->push->setTitle($title);
            $this->push->setMessage($message);
            if ($include_image) {
//                    $push->setImage('http://api.androidhive.info/images/minion.jpg');
            } else {
                $this->push->setImage('');
            }
            $this->push->setIsBackground(FALSE);
            $this->push->setPayload($payload);


            $json = '';
            $response = '';

            if ($push_type == 'topic') {
                $json = $this->push->getPush();
                $response = $this->firebase->sendToTopic('global', $json);
            } else if ($push_type == 'individual') {
                $json = $this->push->getPush();
                $regId = $token;
                $response = $this->firebase->send($regId, $json);
            }
        } else {

            $deviceToken = $token;
            $ctx = stream_context_create();
            // ck.pem is your certificate file
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'devAPNsCertificates.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', '123');
            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            // Create the payload body
            $body['aps'] = array(
                'alert' => array(
                    'title' => 'New Delivery',
                    'body' => 'Admin assigned you a delivery.',
                ),
                'sound' => 'default'
            );
            // Encode the payload as JSON
            $payload = json_encode($body);
            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            // Close the connection to the server
            fclose($fp);
        }*/
        //echo $orderid; die;
        $insertearr = array('order_id' => $orderid, 'to_user_id' => $toId, 'message' => 'Admin assigned you a delivery', 'type' => 'admin');

        $inres = $this->Authentication->insert('notifications', $insertearr);
    
        $customerResult = $this->db->query("select * from orders o left join user u on o.user_id = u.user_id where o.order_id='$orderid'")->row();
            
                $custoken = $customerResult->device_token;
                $custoId = $customerResult->user_id;
$cusdeviceType=$customerResult->device_type;

/*if ($cusdeviceType == 'android') {
            // optional payload
            $payload = array();
            $payload['team'] = '';
            $payload['score'] = '';

            // notification title
            $title = 'Driver Assigned';

            // notification message
            $message = 'A driver assigned to your delivery';

            // push type - single user / topic
            $push_type = 'individual';

            // whether to include to image or not
            $include_image = FALSE;


            $this->push->setTitle($title);
            $this->push->setMessage($message);
            if ($include_image) {
//                    $push->setImage('http://api.androidhive.info/images/minion.jpg');
            } else {
                $this->push->setImage('');
            }
            $this->push->setIsBackground(FALSE);
            $this->push->setPayload($payload);


            $json = '';
            $response = '';

            if ($push_type == 'topic') {
                $json = $this->push->getPush();
                $response = $this->firebase->sendToTopic('global', $json);
            } else if ($push_type == 'individual') {
                $json = $this->push->getPush();
                $regId = $custoken;
                $response = $this->firebase->send($regId, $json);
            }
        } else {

            $deviceToken = $custoken;
            $ctx = stream_context_create();
            // ck.pem is your certificate file
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'devAPNsCertificates.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', '123');
            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            // Create the payload body
            $body['aps'] = array(
                'alert' => array(
                    'title' => 'Driver Assigned',
                    'body' => 'A driver assigned to your delivery.',
                ),
                'sound' => 'default'
            );
            // Encode the payload as JSON
            $payload = json_encode($body);
            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            // Close the connection to the server
            fclose($fp);
        }

*/        $insertearr = array('order_id' => $orderid, 'to_user_id' => $custoId, 'message' => 'A driver assigned to your delivery.', 'type' => 'admin');

        $inres = $this->Authentication->insert('notifications', $insertearr);


        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Vehicle assigned successfully</div>');
            return redirect('admin/orderlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/orderlist');
        }
    }

    public function updateuserstatus($id, $status) {
       // if ($status == 1) {
       //     $status_new = 2;
       // }

      //  if ($status == 2) {
      //      $status_new = 1;
      //  }
        if ($status == 1) {
            $status_new = 2;
            
        }
         elseif($status == 2) {
            $status_new = 1;
        }
       // print_r($id);die;
        $where=array('user_id'=>$id);
        $up = array('user_status'=>$status_new);
        $res = $this->Authentication->update('user',$where,$up);
        if ($res) {
            
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Staus changed successfully</div>');
            return redirect('admin/driverlist');
        } else {
            //echo "t";die;
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/driverlist');
        }
    }


    public function updateadminstatus($id, $status) {
       // if ($status == 1) {
       //     $status_new = 2;
       // }

      //  if ($status == 2) {
      //      $status_new = 1;
      //  }
        if ($status == 1) {
            $status_new = 2;
            
        }
         elseif($status == 2) {
            $status_new = 1;
        }
        $res = $this->Authentication->update('user', array('user_id' => $id), array('user_status' => $status_new));
        if ($res) {
            
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Staus changed successfully</div>');
            return redirect('admin/adminlist');
        } else {
            //echo "t";die;
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/adminlist');
        }
    }

    public function profile() {
        $email = $this->session->userdata('email');
        $password = $this->session->userdata('password');

        //$password=$this->input->post('password');
        $data = $this->Authentication->logincheck($email, $password);

        $data = array('userdetail' => $data);
        $this->load->view('admin/header');
        $this->load->view('admin/profileupdate', $data);
//        $this->load->view('admin/footer');
    }

    public function updateprofile() {
        $post = $this->input->post();
        $id = $this->session->userdata('id');
        if ($this->adminmodal->updateprofile($post, $id)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Profile updated successfully. </div>');
            return redirect('admin/profile');
        }
    }

    public function viewcoupon() {

        $this->load->view('admin/header');
        $this->load->view('admin/viewcoupon');
//        $this->load->view('admin/footer');
    }

    public function viewcoupondata() {

        $var = $this->Authentication->select_data('coupons', '', array('id' => 'desc'));


        //str_replace(array('{', '}'), '', htmlspecialchars(array($arr), ENT_NOQUOTES));
        echo json_encode($var);
    }

    public function delete($id = '', $tablename = '') {
        if ($id == '') {
            $id = $this->input->post('id');
            $table = $this->input->post('table');
            $wherearr = array('order_id' => $id);
        }

        $res = $this->Authentication->delete($table, $wherearr);
        if ($res) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function editcoupon($id) {

        $wherearr = array('id' => $id);
        $var = $this->Authentication->select_data('coupons', $wherearr);
        $arr = array('data' => $var);
        $this->load->view('admin/header');
        $this->load->view('admin/editcoupon', $arr);
//        $this->load->view('admin/footer');
    }

    public function edituser_code($id) {


        $wherearr = array('user_id' => $id);
        //$updatearr=array('coupon_title'=>)
        $post = $this->input->post();
        //var_dump($post);
        $res = $this->Authentication->update('user', $wherearr, $post);
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User updated successfully. </div>');
            return redirect('admin/edituser/'.$id);
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Error occurred pelase try again  </div>');

            return redirect('admin/edituser/'.$id);
        }
    }

public function editdriver_code($id) {


        $wherearr = array('user_id' => $id);
        //$updatearr=array('coupon_title'=>)
        $post = $this->input->post();
        //var_dump($post);
        $res = $this->Authentication->update('user', $wherearr, $post);
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! User updated successfully. </div>');
            return redirect('admin/editdriver/'.$id);
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Error occurred pelase try again  </div>');

            return redirect('admin/editdriver/'.$id);
        }
    }

    public function deleteproduct($id) {
        $post = $this->input->post();

        $res = $this->Authentication->update('products', array('id' => $id), array('status' => 'Deleted'));
        if ($res) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Product Deleted successfully</div>');
            return redirect('admin/productslist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/productslist');
        }
    }

    public function declineorder($id) {
        $post = $this->input->post();

        $res = $this->Authentication->update('orders', array('order_id' => $id), array('delivery_status' => 3));
        if ($res) {


            $qry = "";
        /*    $qryResult = $this->db->query("select * from orders o left join user u on o.driver_id = u.user_id where o.order_id='$id'")->row();
           // echo '<pre>';print_r($qryResult);die;
            $token = $qryResult->device_token;
            $toId = $qryResult->user_id;
            $deviceType = $qryResult->device_type;*/
          /*  if ($deviceType == 'android') {
                // optional payload
                $payload = array();
                $payload['team'] = '';
                $payload['score'] = '';

                // notification title
                $title = 'Cancel Order';

                // notification message
                $message = 'Order has been cancelled';

                // push type - single user / topic
                $push_type = 'individual';

                // whether to include to image or not
                $include_image = FALSE;


                $this->push->setTitle($title);
                $this->push->setMessage($message);
                if ($include_image) {
//                    $push->setImage('http://api.androidhive.info/images/minion.jpg');
                } else {
                    $this->push->setImage('');
                }
                $this->push->setIsBackground(FALSE);
                $this->push->setPayload($payload);


                $json = '';
                $response = '';

                if ($push_type == 'topic') {
                    $json = $this->push->getPush();
                    $response = $this->firebase->sendToTopic('global', $json);
                } else if ($push_type == 'individual') {
                    $json = $this->push->getPush();
                    $regId = $token;
                    $response = $this->firebase->send($regId, $json);
                }
            } else {

                $deviceToken = $token;
                $ctx = stream_context_create();
                // ck.pem is your certificate file
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'devAPNsCertificates.pem');
                stream_context_set_option($ctx, 'ssl', 'passphrase', '123');
                // Open a connection to the APNS server
                $fp = stream_socket_client(
                        'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);
                // Create the payload body
                $body['aps'] = array(
                    'alert' => array(
                        'title' => 'Cancel Order',
                        'body' => 'Order has been cancelled.',
                    ),
                    'sound' => 'default'
                );
                // Encode the payload as JSON
                $payload = json_encode($body);
                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                // Close the connection to the server
                fclose($fp);
            }*/

        /*    $insertearr = array('order_id' => $id, 'to_user_id' => $toId, 'message' => 'Order has been cancelled', 'type' => 'admin');
            $inres = $this->Authentication->insert('notifications', $insertearr);*/

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Order Declined successfully</div>');
            return redirect('admin/orderlist');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-Danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Please try again</div>');
            return redirect('admin/orderlist');
        }
    }

    public function do_upload($file) {
        $config = array(
            'upload_path' => "application/uploads/",
            'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp"
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($file)) {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        } else {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
    }

    public function addcoupon() {
        if (empty($_POST)) {
            $var = $this->Authentication->select_data('categories', '');
            $data = array('category' => $var);
            $this->load->view('admin/header');
            $this->load->view('admin/addcoupon', $data);
//            $this->load->view('admin/footer');
        } else {


            $post = $this->input->post();
            //FOR BANNER IMAGE
            if (!empty($_FILES['banner_image'])) {

                $uplaod_logo = $this->do_upload('banner_image');
                var_dump($uplaod_logo);
                $fullpath = $uplaod_logo['upload_data']['file_name'];

                $banner_url = base_url() . 'uploads/' . $fullpath;
                $post['banner_image'] = $banner_url;
            } else {
                $post['banner_image'] = 'http://retailer.canopussystems.com/application/uploads/icon-no-image.png';
            }

            //FOR COUPON LOGO
            if (!empty($_FILES['coupon_logo'])) {
                //$uplaod = $this->do_upload();
                $uplaod_logo = $this->do_upload('coupon_logo');

                $fullpath_logo = $uplaod_logo['upload_data']['file_name'];
                $coupon_logo = base_url() . 'uploads/' . $fullpath_logo;
                $post['coupon_logo'] = $coupon_logo;
            } else {
                $post['coupon_logo'] = 'http://retailer.canopussystems.com/application/uploads/icon-no-image.png';
            }

            $res = $this->Authentication->insert('coupons', $post);
            if ($res) {

                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Coupon Added successfully. </div>');
                return redirect('admin/viewcoupon');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Error occurred pelase try again  </div>');

                return redirect('admin/addcoupon');
            }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */