<div class="main-content">
    <?php 
  // var_dump($orders);
    ?>
    <div class="panel mb25">
        <div class="panel-heading border">Update Order</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateorder/').'/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ?>" method="post">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup House Number</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_house_number" required="" value="<?php echo $orders[0]->pickup_house_number ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup Street Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_street_name" required=""  value="<?php echo $orders[0]->pickup_street_name ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup Street Suff</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_street_suff" required="" value="<?php echo $orders[0]->pickup_street_suff ?>"> 
                            </div>
                        </div>

                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup Suburb</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_suburb" required=""value="<?php echo $orders[0]->pickup_suburb ?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup State</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_state" required="" value="<?php echo $orders[0]->pickup_state ?>" > 
                            </div>
                        </div>
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup Postcode</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_postcode" required="" value="<?php echo $orders[0]->pickup_postcode ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pickup Country</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pickup_country" required="" value="<?php echo $orders[0]->pickup_country ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff House Number</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_house_number" required="" value="<?php echo $orders[0]->dropoff_house_number ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff Stree Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_stree_name" required="" value="<?php echo $orders[0]->dropoff_stree_name ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff Street Suff</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_street_suff" required="" value="<?php echo $orders[0]->dropoff_street_suff ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff Suburb</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_suburb" required="" value="<?php echo $orders[0]->dropoff_suburb ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff State</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_state" required="" value="<?php echo $orders[0]->dropoff_state ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff Postcode</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_postcode" required="" value="<?php echo $orders[0]->dropoff_postcode ?>" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Dropoff Country</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="dropoff_country" required="" value="<?php echo $orders[0]->dropoff_country ?>" > 
                            </div>
                        </div>
                       
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Product</label>
                            <div class=col-sm-10> 
                                <select name="order_product[]"class="form-control" multiple>
                                    <?php foreach ($allProd as $value) { ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (in_array($value->id, $ordArr)){ ?>selected="" <?php } ?>><?php echo $value->title; ?></option>
                                     <?php   } ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Update Order</button>
                             </div>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       