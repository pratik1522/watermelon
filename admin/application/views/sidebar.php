 <aside class="main-sidebar"> 
    
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="active treeview"> <a href="index-super-admin.php"> <i class="fa fa-dashboard yellow"></i> <span>Dashboard</span> </a> </li>
        <li class="treeview">
              <a href="client_activity.php">
                <i class="fa fa-child "></i>  <span>Activity Log</span>
              </a>
          </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-user"></i> <span>Company</span> <i id="arrowDown" class="fa fa-angle-right pull-right"></i> </a>
          <ul style="display: none;" class="treeview-menu">
            <li><a href="client_list.php"> Company List </a></li>
            <li><a href="add_client.php"> Add New Company</a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-gavel"></i> <span>Order</span> <i id="arrowDown" class="fa fa-angle-right pull-right"></i> </a>
          <ul style="display: none;" class="treeview-menu">
              <li><a href="<?= base_url('admin/orderlist'); ?>"> Order List </a></li>
            <li><a href="create_order.php"> Create an Order/ Quote </a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="report.php"> <i class="fa fa-file-text"></i> <span>Report</span> </a> </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-users"></i> <span>Users</span> <i id="arrowDown" class="fa fa-angle-right pull-right"></i> </a>
          <ul style="display: none;" class="treeview-menu">
            <li><a href="user_list.php"> Users List</a></li>
            
            <li><a href="role_admin.php"> Role Admin </a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-columns"></i> <span>Invoice</span> <i id="arrowDown" class="fa fa-angle-right pull-right"></i> </a>
          <ul style="display: none;" class="treeview-menu">
            <li><a href="invoice_list.php"> Invoice List</a></li>
            <li><a href="invoice_detail.php">Create an Invoice</a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="assign_ftp.php"> <i class="fa fa-circle" aria-hidden="true"></i> <span>Assign FTP Detail</span> </a> </li>
        <li class="treeview"> <a href="manage_documents.php"> <i class="fa fa-folder-o" aria-hidden="true"></i> <span>Manage Documents</span> </a> </li>
        <li class="treeview"> <a href="payment_update.php"> <i class="fa fa-paypal" aria-hidden="true"></i> <span>Payment List</span> </a> </li>
        
        <li class="treeview"> <a href="Estimated_Scheduler.php"> <i class="fa fa-clock-o" aria-hidden="true"></i> <span>Estimated Scheduler</span> </a> </li>
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
  </aside>