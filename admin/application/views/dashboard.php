<div class=row>
    <div class=col-md-3>
        <div>
            <div class="widget bg-white">
                <div class="widget-icon bg-blue pull-left fa fa-microphone"> </div>
                <div class=overflow-hidden> <span class=widget-title>8,372K</span> <span class=widget-subtitle>Registered users</span> </div>
            </div>
            <div class="widget bg-white">
                <div class="widget-icon bg-danger pull-left fa fa-tint"> </div>
                <div class=overflow-hidden> <span class="widget-title percent">86</span> <span class=widget-subtitle>Revenue increase</span> </div>
            </div>
            <div class="widget bg-white">
                <div class="widget-icon bg-success pull-left fa fa-paper-plane"> </div>
                <div class=overflow-hidden> <span class=widget-title>7,355K</span> <span class=widget-subtitle>Pending orders</span> </div>
            </div>
        </div>
    </div>
    <div class=col-md-5>
        <div class="widget-chart bg-white no-padding">
            <div class="row absolute tp lt rt p15">
                <div class=col-xs-12>
                    <div class=pull-right> <i class="fa fa-square text-primary mr5"></i> Mail Server </div>
                    <small class=text-uppercase>Transfer speeds</small> 
                    <h4 class="text-primary bold no-margin">43.02mbps</h4>
                </div>
            </div>
            <div class=rickshaw_graph>
                <div id=dashboard-rickshaw style=height:250px></div>
            </div>
        </div>
    </div>
    <div class=col-md-4>
        <div class="widget-chart bg-white">
            <div class=row>
                <div class=col-xs-12>
                    <small class=text-uppercase>Weekly distribution</small> 
                    <h4 class="no-margin bold text-success">3,490K</h4>
                </div>
            </div>
            <div class="canvas-holder mt5 mb5">
                <div class="flot-pie chart-sm" style=height:171px></div>
            </div>
        </div>
    </div>
</div>