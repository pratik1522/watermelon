 <?php $api_url = $this->config->item('api_url');   ?>
<div class="panel">
   <div class="col-md-12" >
        <br>
     

    </div>
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li><a href="<?php echo site_url('admin/driver_payment'); ?>">Driver payment</a></li>
            <li>Driver payment List</li>
            <li>            
            <!-----file export-->
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                <ul class="dropdown-menu">
                    <li class="divider"></li>                                                
                    <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/xls.png');?>' width="24"/> XLS</a></li>
                  <!--   <li class="divider"></li>
                    <li><a href="#" onClick ="$('#example').tableExport({type:'png',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/png.png');?>' width="24"/> PNG</a></li>
                    <li><a href="#" onClick ="$('#example').tableExport({type:'pdf',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/pdf.png');?>' width="24"/> PDF</a></li> -->
                </ul>
            </div>
            <!--end-->
            </li>
        </ol>           
    </div> 

<style>
.scroll_tabb .table-responsive{overflow-x: scroll;
}
</style>

 
    <div class="panel-body">
    <div class="scroll_tabb">
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered display nowrap" id="example">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Driver ID</th>
                    <th>Payment</th>
                    <th>Payment At</th>                    
                    </tr>
                    </thead>    
            <tbody>
                <?php $i=1; foreach ($users as $key => $value) { ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                    	<td><?php echo $value->driver_id; ?></td>
                        <td><?php echo $value->payment; ?></td>
                        <td><?php echo $value->payment_at; ?></td>                        
                    </tr>
                <?php } ?>
        </table>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentModal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
          <center><h4 class="modal-title">Pay To Driver</h4></center>
        </div>
        <div class="modal-body"> 
            <span style="color:red"><h4>Driver Balance :<span id='bal'></span> </h4></span>  
            <input class="form-control" type="text" name="amount" id='amount' placeholder="Enter amount" required>
            <input type="hidden" id='driver_id'>
            <input type="hidden" id='balance'>
        </div> 
        <div class="modal-footer">
            <input type="button" value="Pay Now" onclick="paynowValidation()" class="btn btn-success pull-right">            
      </div>       
      </div>      
    </div>
</div>


    <script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
    <script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
    <script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
    <script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script> 
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.3/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/tableExport.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jquery.base64.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/html2canvas.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/plugins/tableexport/jspdf/libs/sprintf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/jspdf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/libs/base64.js');?>"></script>  

<script>
$('#new').hide();

$(document).ready(function() {
    $('.table').css('max-height',$(window).height() -340);
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

function paynowValidation(){
    var nowpay      = $('#amount').val();
    var driver_id   = $('#driver_id').val();
    var balance     = $('#balance').val();
    if((balance-nowpay)<0){
        var c = confirm('Pay amount is greater than driver balance. Do you want to continue');        
        if(c==true){
            var r = confirm('Are you want to pay $'+ nowpay+' to driver');
            if (r == true) {
                paynow(nowpay,driver_id);
            }        
        }
    }        
    else{        
        var r = confirm('Are you want to pay $'+ nowpay+' to driver');
        if (r == true) {
            paynow(nowpay,driver_id);
        }
    }
}

function paynow(pay,driver_id){
    if(pay!='' && driver_id!=''){
        $.ajax({
            type:'POST',
            data:{'driver_id':driver_id,'payment':pay},
            url:'<?php echo site_url("Admin/pay_to_driver"); ?>',
            dataType:'json',
            success:function(res){
                if(res.success==1){
                    alert(res.message);
                    location.reload(true);    
                }
                else{
                    alert(res.message);
                }
                console.log(res);
            },
            error:function(res){
                alert(res.message);
                location.reload(true); 
            }
        });

    }
    
}




</script>