  </div>
<footer class=content-footer>
                <nav class=footer-right>
                    <ul class=nav>
                        <li> <a href=javascript:;>Feedback</a> </li>
                        <li> <a href=javascript:; class=scroll-up> <i class="fa fa-angle-up"></i> </a> </li>
                    </ul>
                </nav>
                <nav class=footer-left>
                    <ul class=nav>
                        <li> <a href=javascript:;>Copyright <i class="fa fa-copyright"></i> <span>Urban</span> 2015. All rights reserved</a> </li>
                        <li> <a href=javascript:;>Careers</a> </li>
                        <li> <a href=javascript:;> Privacy Policy </a> </li>
                    </ul>
                </nav>
            </footer>
            <div class=chat-panel>
                <div class=chat-inner>
                    <div class=chat-users>
                        <div class=nav-justified-xs>
                            <ul class="nav nav-tabs nav-justified">
                                <li class=active> <a href=#chat-list data-toggle=tab>Chat</a> </li>
                                <li> <a href=#market data-toggle=tab>Favourites</a> </li>
                                <li> <a href=#activity data-toggle=tab>Activity</a> </li>
                            </ul>
                        </div>
                        <div class=tab-content>
                            <div class="tab-pane active" id=chat-list>
                                <div class=chat-group>
                                    <div class=chat-group-header> Favourites </div>
                                    <a href=javascript:;> <span class=status-online></span> <span>Catherine Moreno</span> </a> <a href=javascript:;> <span class=status-online></span> <span>Denise Peterson</span> </a> <a href=javascript:;> <span class=status-away></span> <span>Charles Wilson</span> </a> <a href=javascript:;> <span class=status-away></span> <span>Melissa Welch</span> </a> <a href=javascript:;> <span class=status-no-disturb></span> <span>Vincent Peterson</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Pamela Wood</span> </a> 
                                </div>
                                <div class=chat-group>
                                    <div class=chat-group-header> Online Friends </div>
                                    <a href=javascript:;> <span class=status-online></span> <span>Tammy Carpenter</span> </a> <a href=javascript:;> <span class=status-away></span> <span>Emma Sullivan</span> </a> <a href=javascript:;> <span class=status-no-disturb></span> <span>Andrea Brewer</span> </a> <a href=javascript:;> <span class=status-online></span> <span>Sean Carpenter</span> </a> 
                                </div>
                                <div class=chat-group>
                                    <div class=chat-group-header> Offline </div>
                                    <a href=javascript:;> <span class=status-offline></span> <span>Denise Peterson</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Jose Rivera</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Diana Robertson</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>William Fields</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Emily Stanley</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Jack Hunt</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Sharon Rice</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Mary Holland</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Diane Hughes</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Steven Smith</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Emily Henderson</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Wayne Kelly</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Jane Garcia</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Jose Jimenez</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Rachel Burton</span> </a> <a href=javascript:;> <span class=status-offline></span> <span>Samantha Ruiz</span> </a> 
                                </div>
                            </div>
                            <div class=tab-pane id=market>
                                <div class=favourite-list>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/avatar.21d1cc35.jpg alt=avatar> 
                                            <div class="status bg-success border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Catherine Moreno</span> <span class=text-muted>Online</span> </div>
                                    </a>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/face1.75317f48.jpg alt=avatar> 
                                            <div class="status bg-success border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Denise Peterson</span> <span class=text-muted>Online</span> </div>
                                    </a>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/face3.0306ffff.jpg alt=avatar> 
                                            <div class="status bg-default border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Charles Wilson</span> <span class=text-muted>Busy</span> </div>
                                    </a>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/face4.cea90747.jpg alt=avatar> 
                                            <div class="status bg-danger border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Melissa Welch</span> <span class=text-muted>Offline</span> </div>
                                    </a>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/face5.535c103a.jpg alt=avatar> 
                                            <div class="status bg-danger border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Vincent Peterson</span> <span class=text-muted>Offline</span> </div>
                                    </a>
                                    <a href=#>
                                        <div class="media-left relative">
                                            <img class="img-circle avatar avatar-xs" src=images/avatar.21d1cc35.jpg alt=avatar> 
                                            <div class="status bg-danger border-white mr10"></div>
                                        </div>
                                        <div class=media-body> <span class=block>Pamela Wood</span> <span class=text-muted>Offline</span> </div>
                                    </a>
                                </div>
                            </div>
                            <div class=tab-pane id=activity>
                                <ol class=activity-feed>
                                    <li class=feed-item> <span>Launched a new application</span> <time datetime="2015-01-30 00:00">2 seconds ago</time> </li>
                                    <li class="feed-item inactive"> <span>Removed chrome from app list</span> <time datetime="2015-01-20 00:00">Jan 20</time> </li>
                                    <li class=feed-item> <span>Approved new user "Jack hunt"</span> <time datetime="2015-01-02 00:00">Jan 02</time> </li>
                                    <li class="feed-item active"> <span>Executed new cron jobs on server with id 67gyu789</span> <time datetime="2014-12-12 00:00">Dec 12</time> </li>
                                    <li class=feed-item> <span class=text>Added paypal to list payment options</span> <time datetime="2014-12-01 00:00">Dec 01</time> </li>
                                    <li class=feed-item> <span>Added 6 new calendar events</span> <time datetime="2014-08-30 00:00">Aug 30</time> </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class=chat-conversation>
                        <div class=chat-header>
                            <a class=chat-back href=javascript:;> <i class="fa fa-angle-left"></i> </a> 
                            <div class=chat-header-title> Charles Wilson </div>
                            <a class=chat-right href=javascript:;> <i class="fa fa-circle-thin"></i> </a> 
                        </div>
                        <div class=chat-conversation-content>
                            <p class="text-center text-muted small text-uppercase bold pb15"> Yesterday </p>
                            <div class="chat-conversation-user them">
                                <div class=chat-conversation-message>
                                    <p>Hey.</p>
                                </div>
                            </div>
                            <div class="chat-conversation-user them">
                                <div class=chat-conversation-message>
                                    <p>How are the wife and kids, Taylor?</p>
                                </div>
                            </div>
                            <div class="chat-conversation-user me">
                                <div class=chat-conversation-message>
                                    <p>Pretty good, Samuel.</p>
                                </div>
                            </div>
                            <p class="text-center text-muted small text-uppercase bold pb15"> Today </p>
                            <div class="chat-conversation-user them">
                                <div class=chat-conversation-message>
                                    <p>Curabitur blandit tempus porttitor.</p>
                                </div>
                            </div>
                            <div class="chat-conversation-user me">
                                <div class=chat-conversation-message>
                                    <p>Goodnight!</p>
                                </div>
                            </div>
                            <div class="chat-conversation-user them">
                                <div class=chat-conversation-message>
                                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class=chat-conversation-footer>
                            <button class=chat-input-tool> <i class="fa fa-camera"></i> </button> 
                            <div class=chat-input contenteditable=""></div>
                            <button class=chat-send> <i class="fa fa-paper-plane"></i> </button> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
        <script src=<?= base_url('vendor/d3/d3.min.js'); ?>></script> 
        <script src=vendor/rickshaw/rickshaw.min.js></script> 
        <script src=<?= base_url('vendor/flot/jquery.flot.js'); ?>></script> 
        <script src=<?= base_url('vendor/flot/jquery.flot.resize.js'); ?>></script>
        <script src=<?= base_url('vendor/flot/jquery.flot.categories.js'); ?>></script> 
        <script src=<?= base_url('vendor/flot/jquery.flot.pie.js'); ?>></script>     
        <script src=<?= base_url('scripts/pages/dashboard.fe7e077d.js'); ?>></script>   
        <script type="text/javascript">
                            /* <![CDATA[ */
                            var _gaq = _gaq || [];
                            _gaq.push(['_setAccount', 'UA-50530436-1']);
                            _gaq.push(['_trackPageview']);

                            (function () {
                                var ga = document.createElement('script');
                                ga.type = 'text/javascript';
                                ga.async = true;
                                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(ga, s);
                            })();

                            (function (b) {
                                (function (a) {
                                    "__CF"in b && "DJS"in b.__CF ? b.__CF.DJS.push(a) : "addEventListener"in b ? b.addEventListener("load", a, !1) : b.attachEvent("onload", a)
                                })(function () {
                                    "FB"in b && "Event"in FB && "subscribe"in FB.Event && (FB.Event.subscribe("edge.create", function (a) {
                                        _gaq.push(["_trackSocial", "facebook", "like", a])
                                    }), FB.Event.subscribe("edge.remove", function (a) {
                                        _gaq.push(["_trackSocial", "facebook", "unlike", a])
                                    }), FB.Event.subscribe("message.send", function (a) {
                                        _gaq.push(["_trackSocial", "facebook", "send", a])
                                    }));
                                    "twttr"in b && "events"in twttr && "bind"in twttr.events && twttr.events.bind("tweet", function (a) {
                                        if (a) {
                                            var b;
                                            if (a.target && a.target.nodeName == "IFRAME")
                                                a:{
                                                    if (a = a.target.src) {
                                                        a = a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);
                                                        b = 0;
                                                        for (var c; c = a[b]; ++b)
                                                            if (c.indexOf("url") === 0) {
                                                                b = unescape(c.split("=")[1]);
                                                                break a
                                                            }
                                                    }
                                                    b = void 0
                                                }
                                            _gaq.push(["_trackSocial", "twitter", "tweet", b])
                                        }
                                    })
                                })
                            })(window);
                            /* ]]> */
        </script>-->