<style type="text/css">
    body { font: normal 10pt Helvetica, Arial; }
    #map { width: 350px; height: 300px; border: 0px; padding: 0px; }
</style>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDexhXkviGf2He0USYlfY2WH-hDH6RRmrM" type="text/javascript"></script>

<script type="text/javascript">
    //Sample code written by August Li
    var icon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/micons/red.png",
            new google.maps.Size(32, 32), new google.maps.Point(0, 0),
            new google.maps.Point(16, 32));
    var center = null;
    var map = null;
    var currentPopup;
    var bounds = new google.maps.LatLngBounds();
    function addMarker(lat, lng, info) {
        var pt = new google.maps.LatLng(lat, lng);
        bounds.extend(pt);
        var marker = new google.maps.Marker({
            position: pt,
            icon: icon,
            map: map
        });
        var popup = new google.maps.InfoWindow({
            content: info,
            maxWidth: 300
        });
        google.maps.event.addListener(marker, "click", function () {
            if (currentPopup != null) {
                currentPopup.close();
                currentPopup = null;
            }
            popup.open(map, marker);
            currentPopup = popup;
        });
        google.maps.event.addListener(popup, "closeclick", function () {
            map.panTo(center);
            currentPopup = null;
        });
    }
    function initMap() {
//alert('ok');
        map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(0, 0),
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },
            navigationControl: true,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.ZOOM_PAN
            }
        });
<?php
foreach ($users as $key => $value) {

//var_dump($row);
    $name = $value->firstname . ' ' . $value->lastname;
    $lat = $value->lattitude;
    $lon = $value->longitude;
    $contact = $value->phone;




    echo("addMarker($lat, $lon, '<b>$name</b><br />$contact');\n");
}
?>
        center = bounds.getCenter();
        map.fitBounds(bounds);

    }
</script>
</head>
<div class=panel>


    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li> <a href=javascript:;>Manage Location</a> </li>

        </ol>

    </div>
    <div class=panel-body>

        <input type="text" class="form-control" placeholder="Enter user name to search" id="search"><br>
        <div id="map" class="col-lg-12" style="width: 100%"></div>
        <button class="btn btn-success" id="showall">Show All Users</button>
         

    </div>
</div>
<script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
<script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
<script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
<script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script>
<script>
    $('#new').hide();
    $(document).ready(function () {

        initMap();
        $('#showall').click(function(){
            initMap();
        });
        $('#search').keyup(function () {
            name = $(this).val();

            $.ajax({url: "<?php echo base_url('admin/search'); ?>", data: {'name': name}, type: 'POST', success: function (result) {
                    str = jQuery.parseJSON(result);
                    for (i = 0; i < str.length; i++)
                    {
                       longitude= str[i].longitude;
                       longitude= str[i].longitude;
                       name= str[i].firstname+' '+str[i].lastname;
                       phone= str[i].phone;
                        map = new google.maps.Map(document.getElementById("map"), {
                            center: new google.maps.LatLng(longitude, longitude),
                            zoom: 18,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
                            },
                            navigationControl: true,
                            navigationControlOptions: {
                                style: google.maps.NavigationControlStyle.ZOOM_PAN
                            }
                        });

                      
                        addMarker(longitude, longitude, '<b>'+name+'</b><br />'+phone);

                        center = bounds.getCenter();
                        map.fitBounds(bounds);
                    }
                }});
        });
    });
</script>