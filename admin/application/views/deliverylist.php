<link rel=stylesheet href=<?= base_url('vendor/datatables/media/css/jquery.dataTables.css'); ?>>
<link rel=stylesheet href=<?= base_url('styles/app.min.df5e9cc9.css'); ?>>
<div class=panel>
    
   
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li> <a href=javascript:;>Delivery List</a> </li>

        </ol>

    </div>
    <div class=panel-body>
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>Title 
                    <th>weight 
                    <th>price 
                    <th>receivers address 
                    <th>Order delivered by 
                    <th>create date
                    <th>Delivery status
                    
            <tbody>
                <?php foreach ($orders as $key => $value) {
                    ?>
                    <tr>
                        <td><?php echo $value->title; ?>
                        <td><?php echo $value->weight; ?>
                        <td><?php echo $value->price; ?> 
                        <td><?php echo $value->receivers_address; ?>
                        <td><?php echo $value->firstname . ' ' . $value->lastname; ?>
                         <td><?php echo $value->create_date; ?>
                        <td><?php if ($value->status == 1) { ?>
                                <span class="label label-warning">Pending</span>
                            <?php } else if ($value->status == 2) { ?>

                                <span class="label label-success">Delivered</span>
                            <?php } else { ?>
                                <span class="label label-danger">Canceled</span>
                            <?php } ?>
              
                        <?php } ?>
        </table>
    </div>
</div>
<script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
<script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
<script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
<script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script>
<script>
    $('#new').hide();
</script>