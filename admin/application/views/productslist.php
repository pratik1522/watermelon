<div class="panel">
   <div class="col-md-12" >
        <br>
        <a href="<?php echo base_url('admin/createproduct'); ?>" class="btn btn-success" role="button" style="float: right">Create New Product</a>

    </div>
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li> <a href=javascript:;>Product List</a> </li>

        </ol>
    </div>
    <div class=panel-body>
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Image </th>
                    <th>Price</th>
                    <th>Colour</th>
                    <th>Description</th>
                    <th>Edit </th>
                    <th>Delete</th>
                        
            <tbody>
                <?php foreach ($products as $key => $value) {
     
                    ?>
                    <tr>
                        <td><?php echo $value->title; ?> </td>
                        <td><img src="<?php echo $value->image; ?>" height="50" width="50" /> </td>
                        <td><?php echo $value->price; ?> </td>
                        <td><?php echo $value->colour; ?> </td>
                        <td><?php echo $value->description; ?> </td>
                        
                        <td>
                            <a href="<?php echo base_url('admin/editproduct').'/'.$value->id; ?>" class=edit>Edit</a> 
                        </td>
                        <td><a id="<?php echo $value->id ?>" href="javascript:void(0)" onclick="confirmation(<?php echo $value->id; ?>)" >Delete</a> 
                        <?php } ?>
        </table>
    </div>
</div>
<script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
<script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
<script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
<script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script> 
<script>
$('#new').hide();

function confirmation($id) {
  var getUrl = window.location;
  var url = getUrl.origin+"/courierapp/admin/deleteproduct/"+$id;
    var r = confirm("Do you really want to delete ?");
    if (r == true) {
        window.location.href=url;
    } 
    
}
</script>