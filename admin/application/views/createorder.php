<div class="main-content">
    <div class="panel mb25">
        <div class="panel-heading border">Create New Order</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/createorder_code') ?>" method="post">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Title</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="title" required="" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Weight</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="weight" required="" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Price</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="price" required="" > 
                            </div>
                        </div>

                        <div class=form-group>
                            <label class="col-sm-2 control-label">Receivers Address</label> 
                            <div class=col-sm-10> 
                                <textarea  class=form-control name="receivers_address" required="" ></textarea> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Pincode</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="pincode" required="" > 
                            </div>
                        </div>
                         <div class=form-group>
                            <label class="col-sm-2 control-label">Order Delivered By</label> 
                            <div class=col-sm-10> 
                                <select  class=form-control name="user_delivery_id" required=""  >
                                    <option value="">Select User</option>
                                    <?php foreach ($data_user as $key => $value) {
                                            
                                         ?>
                                    <option value="<?php echo $value->user_id ?>" ><?php echo $value->firstname.' '.$value->lastname ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Order Delivery Status</label> 
                            <div class=col-sm-10> 
                                <select  class=form-control name="user_delivery_status" required=""  >
                                     <option value="">Select Delivery Status</option>
                                    
                                    <?php
                                    $array=array('1'=>'Pending','2'=>'Delivered','3'=>'Canceled');
                                    foreach ($array as $key => $value) {
                                            
                                         ?>
                                    <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Create Order</button>
                             </div>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       