<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	 function __construct() {
        parent::__construct();
       
        $this->load->model('generalmodel');
    }
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function orderTracking($order_id){		
		if($order_id!=''){ 
			$order_details = $this->generalmodel->getSingleRecord('orders',array('order_id'=>$order_id));
			//echo '<pre>';
			//print_r($order_details);die();
			if(!empty($order_details)){
				$pickup = $order_details->pickup_house_number.', '.$order_details->pickup_street_name.', '.$order_details->pickup_suburb.', '.$order_details->pickup_state;
				$dropoff = $order_details->dropoff_house_number.', '.$order_details->dropoff_stree_name.', '.$order_details->dropoff_suburb.', '.$order_details->dropoff_state;
				$marker[]  = array($order_details->pickupaddress,$order_details->pickup_lat,$order_details->pickup_long,'p');
				$marker[] = array($order_details->dropoffaddress,$order_details->dropoff_lat,$order_details->dropoff_long,'d');
				$markers =  json_encode($marker);
				$data['book'] = $order_details;
				$data['marker'] = $markers;
				$this->load->view('orderTracking',$data);
			}
			else{
				$data[]= array('error'=>1,'message'=>'Order is not found.');
				$this->load->view('orderTracking',$data);
			}
		}
		else{
			redirect(site_url("Welcome/unknow"));
		}
	}

	public function driverLiveLocation(){
        if(isset($_POST['driver_id']) && $_POST['driver_id']!='' && isset($_POST['order_id']) && $_POST['order_id']!=''){
            extract($_POST);
            $live = $this->generalmodel->getSingleRecord('user',array('user_id'=>$driver_id)); 
            //print_r($live);die();
            if(!empty($live))
            { 
                $order 	  =  $this->generalmodel->getSingleRecord('orders',array('order_id'=>$order_id));  
                $status   =  $order->delivery_status;
                $vehicle  =  $order->vehicle_type;
                $driver_name = $live->firstname.' '.$live->lastname;
                $marker = array($driver_name,$live->latitude,$live->longitude,'driver',$status,$vehicle);                               
                echo json_encode($marker);                
            }
            else{
            	$response = array('error'=>1,'message'=>'Driver is not found');
            	echo json_encode($response);
            }
        }
        else{
            $response = array('error'=>1,'message'=>'access denied');
            echo json_encode($response);
        }
    }
}
