<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class StandardModel extends CI_Model
{ 
	
	public function __construct()
    {
		parent::__construct();
		//load database library
        $this->load->database(); 
		$this->load->library('email');
		
		/*if($this->session->userdata('ses_time_zone'))
		{
			date_default_timezone_set($this->session->userdata('ses_time_zone'));
		}*/
    }
	
		function getdriverratingDetailsById($driver_id)
	{
		$aaa='';
		//$this->db->select("sum(ratevalue) as rate");
		$this->db->select("sum(driverRatting) as rate,feedback,count(id) as totalid");
		$this->db->from("review");
		
		if($driver_id!='')
		{
		 $this->db->where('driver_id',$driver_id);
		}

		 $query = $this->db->get();// execute
		
		if($query->num_rows()>0)
		{
			return $query->result();// select All value from table
			//return $query->row;// select single row from table
			//return true;
		}
		else
		{
			return false;
		}
	}

	
	
			 public function getSingleRecord($table_name,$where)
    {
	return $this->db->get_where($table_name,$where)->row();
    }
	
//------------------------Fuction For Insert Data in to Table------------------ 
	function insert_query($tbl_name,$data)
	{
		
		$this->db->insert($tbl_name,$data);
		return $this->db->insert_id();// mysql_insert_id();
	}

//------------- Select all data from table or single data-----------------------
	function select_query($tbl_name,$where=false)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		
		if($where!='')
		{
			$this->db->where($where);
		}
		$query = $this->db->get();// execute
		if($query->num_rows()>0)
		{
			return $query->result();// select All value from table
			//return $query->row;// select single row from table
			//return true;
		}
		else
		{
			return false;
		}
	}
public function user_deliveries($user_id){

      $this->db->select('user.firstname,lastname,phone,profile_image,orders.order_id,orders.vehicle_type,dropoff_mob_number,dropoff_last_name,dropoff_first_name,driver_id,delivery_type,delivery_cost,delivery_date,pickupaddress,dropoffaddress,created_at');
      $this->db->from('orders');
  //    $where="orders.user_id='".$user_id."' AND delivery_status='5' OR delivery_status='6' OR delivery_status='7' OR delivery_status='1'";
          $where="orders.user_id='".$user_id."' AND delivery_status IN(5,6,7,1)";
       //$where = "userid='".$user_id."' AND status='1' OR status='2'";	
      $this->db->where($where);
       
      $this->db->join('user', 'user.user_id=orders.driver_id','left');
	  $this->db->order_by('orders.order_id','DESC'); 
	  	$query = $this->db->get();// execute
	//	echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			return $query->result();
		}

      

}

public function selectquery($order_id){

      $this->db->select('*');
      $this->db->from('orders');
      $this->db->join('user', 'orders.user_id = user.user_id','left');
      $this->db->where('orders.order_id',$order_id);
        $this->db->order_by('orders.order_id','DESC'); 
	  	$query = $this->db->get();// execute
	//	echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			return $query->row();
		}

}

public function notificationlist($user_id){

      $this->db->select('orders.*,notifications.message,user.firstname,user.lastname,user.profile_image');
      $this->db->from('notifications');
      $this->db->where('notifications.to_user_id',$user_id);
      $this->db->join('orders', 'orders.order_id=notifications.order_id','left');
      $this->db->join('user', 'notifications.from_user_id=user.user_id','left');
      $this->db->order_by('orders.order_id','DESC'); 
	  	$query = $this->db->get();// execute
	//	echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			return $query->result();
		}
 }

public function neardriveracc($lat1,$lon1,$vehicletype){

	$distance = 10;
	$this->db->select("*,( 3959 * acos( cos( radians($lat1) ) * cos( radians(`pickup_lat`) ) * cos( radians( `pickup_long` ) - radians($lon1) ) + sin( radians($lat1) ) * sin( radians(`pickup_lat`) ) ) ) AS distance");
	 $this->db->from('orders');
	 $this->db->where('delivery_status',5);
	 $this->db->where('vehicle_type',$vehicletype);
	 $this->db->where('driver_id',0);
	 $this->db->having('distance <= ',7);
	 $this->db->order_by('distance');
	 //$this->db->limit(20, 0);
	 return $this->db->get()->result(); 
}

public function nearbydeliveryboys($lat1,$lon1,$vehicletype){

	$distance = 10;
	$this->db->select("*,( 3959 * acos( cos( radians($lat1) ) * cos( radians(`latitude`) ) * cos( radians( `longitude` ) - radians($lon1) ) + sin( radians($lat1) ) * sin( radians(`latitude`) ) ) ) AS distance");
	 $this->db->from('user');
	 $this->db->where('user_type',2);
	 $this->db->where('login_status','yes');
	 $this->db->having('distance <= ',7);
	 $this->db->order_by('distance');
	 //$this->db->limit(20, 0);
	 return $this->db->get()->result(); 
}


public function neardelivery($lat1,$lon1){

	$distance = 10;
	$this->db->select("*,( 3959 * acos( cos( radians($lat1) ) * cos( radians(`latitude`) ) * cos( radians( `longitude` ) - radians($lon1) ) + sin( radians($lat1) ) * sin( radians(`latitude`) ) ) ) AS distance");
	 $this->db->from('user');
	 $this->db->where('user_type',2);
//	 $this->db->where('login_status','yes');
	 $this->db->having('distance <= ',7);
	 $this->db->order_by('distance');
	 //$this->db->limit(20, 0);
	 return $this->db->get()->result(); 
}
	
		
//-------------- Select single data with where condition from table-------------
	function select_rowwhere_query($tbl_name,$where=false)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		
		if($where!='')
		{
	    	$this->db->where($where);// array('f1'=>$f1,'f2'=>$f2)
		}
	
		$query = $this->db->get();// execute
		if($query->num_rows()>0)
		{
			return $query->row();// select single row from table
		}
		
		else
		{
			return false;
		}
	}
	
//-----------------------------select function with count,where,join,limit,offset,orderby,like----------------------------------------------------------	
		   
	function select($tbl_name_enc,$select='*',$record_type,$count_status,$where=false,$offset=false,$limit=false,$orderby=false,$order=false,$like=false,$join=false)
	{
				
		$this->db->select($select); //seslect all record
		$this->db->from($tbl_name_enc); //define record from which table
		
		if($join)
		{
			foreach($join as $key_j=>$value_j)
			{
				$this->db->join($key_j,$value_j);  //condition
			}
			
		}
		
		if($where)
		{
			$this->db->where($where); //condition
		}
		if($like)
		{
			foreach($like as $key=>$value)
			{
				$this->db->like($key,$value);  //condition
			}
		}
			
		if($order&&$orderby)
		{
			$this->db->order_by($orderby,$order);
		}
		
		if($count_status=='f'&&$limit)
		{
			$this->db->limit($limit,$offset);// define limit for redord on page
		}
			
		
		$query = $this->db->get();
		
	 	$num_rows=$query->num_rows();
		
		if($count_status=='f')
		{
			
			if($num_rows>0)
			{
				if($record_type=='s')
				{
					
					if(!empty($like_name)&&!empty($like_value))
					{
						$data=$query->row();
						$data=$this->filter_search($like_name,$like_value,$data,$record_type);
					}
					else
					{
						return $query->row();
					}
					
				}
				else
				{
					$data=$query->result();
					return $data; 
					
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(!empty($like_name)&&!empty($like_value))
			{
				$data=$query->result();
				return count($this->filter_search($like_name,$like_value,$data,$record_type));
			}
			else
			{
				return $num_rows;
			}
			
		}
		
		
	} 


//----------------------------------------------------------------------------------------------------------------------------------------------------
		   
	
//---------------Funtion for Update Data from Table-----------------------------	
	function update_query($tbl_name,$up_data,$where)
	{
		$this->db->where($where);
		return $this->db->update($tbl_name,$up_data);
	}
//---------------Funtion for Delete Data from Table-----------------------------	
	function delete_query($tbl_name,$where)
	{
		$this->db->where($where);
		$this->db->delete($tbl_name);
		return true;
	}
//---------------Funtion for Count Number of Data in Table--------------------------	
	function count_data($tabl_name,$where=false)
	{
		$this->db->select("*");
		$this->db->from($tabl_name);
		if($where!='')
		{
			$this->db->where($where);
		}
		$query = $this->db->get();// execute
		if($query->num_rows()>0)
		{
			return count($query->result());
		}
	}
	//--------------------------get blocked Users list---------------------------	
	function select_blocked_users($offset,$limit)
	{	
		$this->db->select('DISTINCT(tbl_user_like.user_to_id),tbl_users.user_id,user_fullname,user_email,user_country,is_vip,coins,user_profile_pic');
		$this->db->from('tbl_users');
		
		$where=array('tbl_user_like.like_status'=>2);   
		$this->db->where($where);
		$this->db->join('tbl_user_like', 'tbl_user_like.user_to_id = tbl_users.user_id');
		$this->db->order_by('tbl_users.user_id','DESC');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();// execute
		//echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
//--------------------------Function For Pagingation---------------------------	
	function paging_data($tabl_name,$where=false,$orderby=false,$order=false,$offset=0,$limit=3)
	{
		$this->db->select("*");
		$this->db->from($tabl_name);
		if($where!='')
		{
			$this->db->where($where);
		}
		if($order&&$orderby)
		{
			$this->db->order_by($orderby,$order);
		}
		
		
		$this->db->limit($limit,$offset);
		$query = $this->db->get();// execute
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
//-------------------------Function For Mysql Injection------------------------	
	function sql_injection($string)
	{
		return mysql_real_escape_string($string);	
	}
//----------------------------------------------------------------------------------	 
	 
	 function cdf($date)//current date from time stamptodate
	 {
		return date("d-M-Y",strtotime($date));
	 }
	 
	
	// prevent from sql injection 
	function go_safe($string,$string1=false)
	{
		if($string1==true)
		{
			return mysql_real_escape_string(trim($string));
		}
		else
		{
			return mysql_real_escape_string(trim($string));
		}
	}
	
	
	function insert($tbl_name,$data)
	{
		$this->db->insert($tbl_name,$data);
		return $this->db->insert_id();
	}
	
	
	function c_to_md5($string,$array=false)
	{
		return  md5($string);
	}
	


	

	
	function getRouteDetails($lat1,$lon1,$lat2,$lon2)
	{
		
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lon1."&destinations=".$lat2.",".$lon2."&mode=driving&language=pl-PL";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		
		//echo '<pre>';print_r($response_a);die;
		
		$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
		$miles=$dist*0.621371;
		$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
		$results=array('distance' => $miles, 'time' => $time);
		//print_r($results);die;
		return $results;
	}
	
	function fetchCountryByLatLong($lat,$lng)
	{
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
	$json = @file_get_contents($url);
	$output=json_decode($json);
	//echo "<pre>";print_r($output);die;
	$status = $output->status;
	
	for($j=0;$j<count($output->results[0]->address_components);$j++){
    	$cn=array($output->results[0]->address_components[$j]->types[0]);
		    if(in_array("country", $cn)){
		        $country= $output->results[0]->address_components[$j]->long_name;
				
		    }
			
	}
	if($status=="OK")
	//return $data->results[0]->formatted_address;
	return $country;
	else
	return '';
	
	}
	
	function fetchCityByLatLong($lat,$lng)
	{
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
	$json = @file_get_contents($url);
	$output=json_decode($json);
	//echo "<pre>";print_r($output);die;
	$status = $output->status;
	
	for($j=0;$j<count($output->results[0]->address_components);$j++){
    	$cn=array($output->results[0]->address_components[$j]->types[0]);
		    if(in_array("locality", $cn)){
		        $city= $output->results[0]->address_components[$j]->long_name;
		    }
			
	}
	if($status=="OK")
	//return $data->results[0]->formatted_address;
	return $city;
	else
	return '';
	
	}
	
	
	
	//------------------------driver payments------------------ 
	function driverPayment($driverId,$x_amount,$transaction_Id,$approval_Code)
	{
		$created_on  =date('Y-m-d H:i:s');
		$data=array('driverId'=>$driverId,'x_amount'=>$x_amount,'transaction_Id'=>$transaction_Id,'approval_Code'=>$approval_Code,'createdOn'=>$created_on);
		
		$subEndDate=date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s'))) . " + 365 day"));
		$dataX=array('driverId'=>$driverId,'subStartDate'=>$created_on,'subEndDate'=>$subEndDate,'createdOn'=>$created_on);
		if($this->db->insert('driverPayments',$data))
		{
			$where=array('driverId'=>$driverId);
			$isSub=$this->select_rowwhere_query('driverSubcription',$where);
			
			if($isSub){
				
				  $this->db->where($where);
                $this->db->update('driverSubcription',$dataX); 
				  return  "update";//$this->db->affected_rows() mysql_insert_id(); 	
				
			}else{
				
				 $this->db->insert('driverSubcription',$dataX);
				
				 return  "insert";//$this->db->insert_id(); mysql_insert_id();
			}
		  
	    }else{
	    	
			return FALSE;
	    }
	}
	
	// check if email is unique
	public function isEmailUnique($table,$where){
		$this->db->where($where);
		$this->db->from($table);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	// check if email is unique
	public function isSocialIdUnique($table,$where){
		$this->db->where($where);
		$this->db->from($table);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	//get Rang
	public function getRangByAdmin()
	{
		$this->db->select('rang_value');
		$this->db->from('`tbl_user_rang');
		$query = $this->db->get();
		return $query->row()->rang_value;
	}
	
	//Param validation
    function param_validation($paramarray, $data)
    {
        $returnArr = array();
        $NovalueParam = array();
        foreach ($paramarray as $val)
        {
            if (!$data[$val])
            {
                $NovalueParam[] = $val;
            }
        }
        if (is_array($NovalueParam) && count($NovalueParam) > 0)
        {
            $returnArr['status'] = 0;
            $returnArr['message'] = 'Sorry, that is not valid input. You missed ' . implode(',', $NovalueParam) . ' parameters';
            return $returnArr;
        }
        else
        {
            return $returnArr;
        }
    }
	
	function validateEmail($email) {
      return filter_var($email, FILTER_VALIDATE_EMAIL);
   }
   
    
	
	public function getNotificationList($user_id,$isUserVip)
    {
    	
		 $loginUserDetails=getUserDetailsById($user_id);
		 $user_lat=$loginUserDetails['user_lat'];
		 $user_long=$loginUserDetails['user_long'];
		
		
    	$token=1;
		$i=0;
		$j=0;
    	
    		//--------------if token  0-----------------------------------------------------------------------------------------------  
	        $this->db->select('*');
			$where=array('user_id_to'=>$user_id);
	        $this->db->where($where);
	        $this->db->from('tbl_notification');
			$this->db->order_by('createdOn','DESC');
	        $datas = $this->db->get();
	        $data= $datas->result_array(); 
	        //echo '<pre>'; print_r($data);die;
	        $arraycount=count($data);
	        $respData= array();
	        $AllUsers= array();
			if(!empty($data))
			{
				foreach($data as $userDatas)
		        {						
					
			    	//echo $miles.'<br>';
					   //if($miles<=$rang)
					    //{
							 $like_user_id= $userDatas['user_id_from'];
							
							 $likeStatus=$this->getUserLikeStatus($user_id,$like_user_id); 
							 //echo $likeStatus; echo '<br>';
							 if($likeStatus ==1 || !$likeStatus){
							 	
							 	 $userDetails=getUserDetailsById($like_user_id);
								 $mutualStatus=$this->checkMutualStatus($user_id,$like_user_id);
                                 $respData['isMutual']=$mutualStatus;
								 $respData['user_list_id']=$like_user_id;
								 $respData['user_fullname']=$userDetails['user_fullname'];
								 $respData['user_city']=$userDetails['user_city'];
								 $respData['user_country']=$userDetails['user_country'];
								 $respData['is_online']=$userDetails['is_online'];
								 $respData['user_age']=$userDetails['user_age'];
								 
								//---------get miles--------------------
							 	$theta = $user_long - $userDetails['user_long'];
							    $dist = sin(deg2rad($user_lat)) * sin(deg2rad($userDetails['user_lat'])) +  cos(deg2rad($user_lat)) * cos(deg2rad($userDetails['user_lat'])) * cos(deg2rad($theta));
							    $dist = acos($dist);
							    $dist = rad2deg($dist);
						    	$miles = $dist * 60 * 1.1515;
							 	
								 
								 $respData['miles']=number_format($miles,4);
								// $respData['friends_count']=0;
								 $respData['likeStatus']=$likeStatus;
								// $respData['status']=$userDetails['status'];
								 $respData['user_profile_pic']=$userDetails['user_profile_pic'];
								 $respData['noti_date']=$userDatas['createdOn'];
								 if($isUserVip=='1'){
								 	 $respData['noti_txt']=$userDatas['noti_txt'];
								 }else{
								 	 $respData['noti_txt']=$userDatas['noti_txt_nonvip'];
								 }
								
								 
								
								
								 $AllUsers[]= $respData;
							 }
						//}
							
				  }
			      //die;
				
					
    	
			}else{
				return FALSE;
			}
			return $AllUsers;
			//--------------------------end of token  0------------------------------------------------------
    		
	}
	
	
	
   public function getUserLikeStatus($user_id,$like_user_id)
   {
	   $where=array('user_id'=>$user_id,'user_to_id'=>$like_user_id);
	   $likeStatus=$this->select('tbl_user_like','*','s','f',$where,false,false,false,false);
	  // echo '<pre>';print_r($likeStatus);die;
	    if(!empty($likeStatus))
	    {
	    	
	    		return $likeStatus->like_status;
	    	
	    
		}else{
        	  return 0;  
	        }   
   }
   //getLikeDateTime
   public function getLikeDateTimeWhoLikeMe($user_id,$user_who_like_me)
   {
       $current_date=date('Y-m-d');
	   $where=array('user_id'=>$user_who_like_me,'user_to_id'=>$user_id);
	   $likeStatus=$this->select('tbl_user_like','*','s','f',$where,false,false,false,false);
	  // echo '<pre>';print_r($likeStatus);die;
	    if(!empty($likeStatus))
	    {
	    	//$likeStatus->createdOn;
			$likeDate=date('Y-m-d',strtotime($likeStatus->createdOn));
	    	//echo $likeDate.'<br>'.$current_date;
	    	if($likeDate==$current_date){
	    		return 'Today';
	    	}else{
	    		return date('F j, Y',strtotime($likeStatus->createdOn));
	    	}
		}else{
        	  return 0;  
	        }   
   }
   
   public function getLikeDateTimeWhoIlike($user_id,$user_who_i_like)
   {
       $current_date=date('Y-m-d');
	   $where=array('user_id'=>$user_id,'user_to_id'=>$user_who_i_like);
	   $likeStatus=$this->select('tbl_user_like','*','s','f',$where,false,false,false,false);
	  // echo '<pre>';print_r($likeStatus);die;
	    if(!empty($likeStatus))
	    {
	    	//$likeStatus->createdOn;
			$likeDate=date('Y-m-d',strtotime($likeStatus->createdOn));
	    	//echo $likeDate.'<br>'.$current_date;
	    	if($likeDate==$current_date){
	    		return 'Today';
	    	}else{
	    		return date('F j, Y',strtotime($likeStatus->createdOn));
	    	}
		}else{
        	  return '0';  
	        }   
   }
   
   //my chat user list starts
     public function getMychatUsers($list_type,$user_id){
		if($list_type==1)//get mutual chat users
		{
			 $where=array('is_mutual'=>1,'user_id'=>$user_id,'user_friend_id <>'=>$user_id,);
	   		 $users=$this->select('tbl_chat_users','user_friend_id','m','f',$where);
			  //echo $this->db->last_query();die;
				foreach($users as $userDatas)
		        {
					$user_list_id= $userDatas->user_friend_id;
					$userDetails=getUserDetailsById($user_list_id);
					$respData['user_list_id']=$user_list_id;
					$respData['user_fullname']=$userDetails['user_fullname'];
					//$respData['quickbloxId']=$userDetails['quickbloxId'];
					//$respData['quickbloxPassword']='wink@gst2016';
					$respData['is_online']=$userDetails['is_online'];
				    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
				    $respData['user_email']=$userDetails['user_email'];
					$AllUsers[]= $respData;
					
						
				}
		
		}else{// get all chat users
			 //$where=array('is_mutual'=>2);
			 $where=array('user_id'=>$user_id,'user_friend_id <>'=>$user_id,);
	   		 $users=$this->select('tbl_chat_users','user_friend_id','m','f',$where);
			  //echo $this->db->last_query();die;
				foreach($users as $userDatas)
		        {
		        	
					
					$user_list_id= $userDatas->user_friend_id;
					$userDetails=getUserDetailsById($user_list_id);
					$respData['user_list_id']=$user_list_id;
					$respData['user_fullname']=$userDetails['user_fullname'];
					//$respData['quickbloxId']=$userDetails['quickbloxId'];
					//$respData['quickbloxPassword']='wink@gst2016';
					$respData['is_online']=$userDetails['is_online'];
				    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
				    $respData['user_email']=$userDetails['user_email'];
					$AllUsers[]= $respData;
					
						
				}
 	
		}
		if(!empty($AllUsers)){
			
			 return $AllUsers;
		}else{
			 return FALSE;
		}	
}
   //my chat user list ends
   ///Get Chat List
   public function getchatList($list_token,$list_type,$user_id){
		$token=1;
		$i=0;
		$j=0;
		if($list_type==1)
		{
			//All user online and offline
			if($list_token=='not_token')
		    {
					//--------------if token  0-----------------------------------------------------------------------------------------------  
					$this->db->select('*');
					$where=array('user_status'=>1 ,'user_id <>'=>$user_id);
			        $this->db->where($where);
			        $this->db->from('tbl_users');
			        $datas = $this->db->get();
			        $data= $datas->result_array(); 
			        //echo $this->db->last_query();
			       // echo '<pre>'; print_r($data);die;
			        $arraycount=count($data);
			        $respData= array();
			        $AllUsers= array();
					if(!empty($data))
					{
						
						foreach($data as $userDatas)
				        {
				        	//$mutualStatus=$this->checkMutualStatus($user_id,$like_user_id); 
							//$respData['isMutual']=$mutualStatus;
							
							$user_list_id= $userDatas['user_id'];
							$userDetails=getUserDetailsById($user_list_id);
							
							$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
							$respData['isMutual']=$mutualStatus;
							
							$respData['user_list_id']=$user_list_id;
							$respData['user_fullname']=$userDetails['user_fullname'];
							$respData['is_online']=$userDetails['is_online'];
						    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
						    $respData['user_email']=$userDetails['user_email'];
							$AllUsers[]= $respData;
							
							if($token===10){break;}else{ $token++;}
							$i++;	
						}
						if($arraycount>=10){
							$AllUsers['list_token']=$data[$i]['createdOn'];
						}else{
							$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
						}
					}
					else{
						$AllUsers['list_token']=date('Y-m-d H:i:s',time());
					
					}
				     // echo '<pre>'; print_r($AllUsers);die;
					return $AllUsers;
					//--------------------------end of token  0------------------------------------------------------
			}
			else
			{
				//--------------if token is not 0-----------------------------------------------------------------------------------------------  
				$this->db->select('*');
				$where=array('user_status'=>1,'user_id <>'=>$user_id,'createdOn >'=>$list_token);
				$this->db->where($where);
				$this->db->from('tbl_users');
				$datas = $this->db->get();
				$data= $datas->result_array(); 
				//echo '<pre>'; print_r($data);die;
				$arraycount=count($data);
				$respData= array();
				$AllUsers= array();
				if(!empty($data))
					{
						
						foreach($data as $userDatas)
				        {
							$user_list_id= $userDatas['user_id'];
							$userDetails=getUserDetailsById($user_id);
							
							$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
							$respData['isMutual']=$mutualStatus;
							
							
							$respData['user_list_id']=$user_list_id;
							$respData['user_fullname']=$userDetails['user_fullname'];
							$respData['is_online']=$userDetails['is_online'];
						    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
						    $respData['user_email']=$userDetails['user_email'];
							$AllUsers[]= $respData;
							
							if($token===10){break;}else{ $token++;}
							$i++;	
						}
						if($arraycount>=10){
							$AllUsers['list_token']=$data[$i]['createdOn'];
						}else{
							$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
						}
					}
					else{
						$AllUsers['list_token']=date('Y-m-d H:i:s',time());
					
					}
				    // echo '<pre>'; print_r($AllUsers);die;
					return $AllUsers;
					 //--------------------------end of token not 0------------------------------------------------------
			}
			
		}elseif($list_type==2)
		{
			/// only online user
			if($list_token=='not_token')
		    {
					//--------------if token  0-----------------------------------------------------------------------------------------------  
					$this->db->select('*');
					$where=array('user_status'=>1,'user_id <>'=>$user_id,'is_online'=>1);
			        $this->db->where($where);
			        $this->db->from('tbl_users');
			        $datas = $this->db->get();
			        $data= $datas->result_array(); 
			        //echo $this->db->last_query();
			       // echo '<pre>'; print_r($data);die;
			        $arraycount=count($data);
			        $respData= array();
			        $AllUsers= array();
					if(!empty($data))
					{
						
						foreach($data as $userDatas)
				        {
							$user_list_id= $userDatas['user_id'];
							$userDetails=getUserDetailsById($user_id);
							$respData['user_list_id']=$user_list_id;
							
							$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
							$respData['isMutual']=$mutualStatus;
							
							
							$respData['user_fullname']=$userDetails['user_fullname'];
							$respData['is_online']=$userDetails['is_online'];
						    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
						    $respData['user_email']=$userDetails['user_email'];
							$AllUsers[]= $respData;
							
							if($token===10){break;}else{ $token++;}
							$i++;	
						}
						if($arraycount>=10){
							$AllUsers['list_token']=$data[$i]['createdOn'];
						}else{
							$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
						}
					}
					else{
						$AllUsers['list_token']=date('Y-m-d H:i:s',time());
					
					}
				     // echo '<pre>'; print_r($AllUsers);die;
					return $AllUsers;
					//--------------------------end of token  0------------------------------------------------------
			}
			else
			{
				//--------------if token is not 0-----------------------------------------------------------------------------------------------  
				$this->db->select('*');
				$where=array('user_status'=>1,'createdOn >'=>$list_token,'user_id <>'=>$user_id,'is_online'=>1);
				$this->db->where($where);
				$this->db->from('tbl_users');
				$datas = $this->db->get();
				$data= $datas->result_array(); 
				//echo '<pre>'; print_r($data);die;
				$arraycount=count($data);
				$respData= array();
				$AllUsers= array();
				if(!empty($data))
					{
						
						foreach($data as $userDatas)
				        {
							$user_list_id= $userDatas['user_id'];
							$userDetails=getUserDetailsById($user_id);
							
							$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
							$respData['isMutual']=$mutualStatus;
							
							
							$respData['user_list_id']=$user_list_id;
							$respData['user_fullname']=$userDetails['user_fullname'];
							$respData['is_online']=$userDetails['is_online'];
						    $respData['user_profile_pic']=$userDetails['user_profile_pic'];
						    $respData['user_email']=$userDetails['user_email'];
							$AllUsers[]= $respData;
							
							if($token===10){break;}else{ $token++;}
							$i++;	
						}
						if($arraycount>=10){
							$AllUsers['list_token']=$data[$i]['createdOn'];
						}else{
							$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
						}
					}
					else{
						$AllUsers['list_token']=date('Y-m-d H:i:s',time());
					
					}
				    // echo '<pre>'; print_r($AllUsers);die;
					return $AllUsers;
					 //--------------------------end of token not 0------------------------------------------------------
			}
		}else{
			//get mutual online
					if($list_token=='not_token')
			    	{
			    		
			    		$this->db->select('user_to_id');
				        $this->db->where('user_id',$user_id);
				        $this->db->from('tbl_user_like');
				        $dataV = $this->db->get();
				        $visitorList= $dataV->result_array(); 
			    		//echo '<pre>';print_r($visitorList);die;
						$uids = Array();
						foreach($visitorList as $u) $uids[] = $u['user_to_id'];
						$list = implode(",",$uids);
			    		$v=explode(",",$list);
			    		$vi=array_values($v);
			    		//print_r($vi);die;
			    		//--------------if token  0-----------------------------------------------------------------------------------------------  
				        $this->db->select('user_id,createdOn');
						$where=array('user_to_id'=>$user_id);
				        $this->db->where($where);
				        $this->db->where_in('user_id',$vi);
				        $this->db->from('tbl_user_like');
				        $datas = $this->db->get();
				        $data= $datas->result_array(); 
				        //echo $this->db->last_query();
				        //echo '<pre>'; print_r($data);die;
				        
				        $arraycount=count($data);
				        $respData= array();
				        $AllUsers= array();
						if(!empty($data))
						{
							//--------loged in user details-------------------------------
							 $userDetailsLog=getUserDetailsById($user_id);
							 $user_lat=$userDetailsLog['user_lat'];
						     $user_long=$userDetailsLog['user_long'];
							//------------------------------------------------------------
							foreach($data as $userDatas)
					        {
					        	
							  		$user_list_id= $userDatas['user_id'];
									$likeStatus=$this->getUserLikeStatus($user_id,$user_list_id); 
									 //echo $likeStatus; echo '<br>';
										 if($likeStatus ==1 || !$likeStatus){
											$userDetails=getUserDetailsById($user_list_id);
											//$respData['is_vip']=$userDetails['is_vip'];
											$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
											$respData['isMutual']=$mutualStatus;
											
											$respData['user_list_id']=$user_list_id;
											$respData['user_fullname']=$userDetails['user_fullname'];
											$respData['is_online']=$userDetails['is_online'];
											$respData['user_profile_pic']=$userDetails['user_profile_pic'];
											$respData['user_email']=$userDetails['user_email'];
											$AllUsers[]= $respData;
									 }
								if($token===10){break;}else{ $token++;}
								$i++;	
							  }
						      //die;
							
								if($arraycount>=10){
										$AllUsers['list_token']=$data[$i]['createdOn'];
									 }else{
										$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
									 }
			    	
						}else{
							$AllUsers['list_token']=date('Y-m-d H:i:s',time());
						}
						//print_r($AllUsers);die;
						return $AllUsers;
						//--------------------------end of token  0------------------------------------------------------
							
			    	}
			    	else{
							$this->db->select('user_to_id');
					        $this->db->where('user_id',$user_id);
					        $this->db->from('tbl_user_like');
					        $dataV = $this->db->get();
					        $visitorList= $dataV->result_array(); 
				    		//echo '<pre>';print_r($visitorList);
							$uids = Array();
							foreach($visitorList as $u) $uids[] = $u['user_to_id'];
							$list = implode(",",$uids);
				    		$v=explode(",",$list);
				    		$vi=array_values($v);
							//--------------if token is not 0-----------------------------------------------------------------------------------------------  
					        $this->db->select('*');
							$where=array('user_status'=>1,'createdOn >'=>$list_token,'user_id <>'=>$user_id);
					        $this->db->where($where);
							$this->db->where_in('user_id',$vi);
					        $this->db->from('tbl_users');
					        $datas = $this->db->get();
					        $data= $datas->result_array(); 
					        //echo '<pre>'; print_r($data);die;
					        $arraycount=count($data);
					        $respData= array();
					        $AllUsers= array();
							if(!empty($data)){
								//--------loged in user details-------------------------------
								 $userDetailsLog=getUserDetailsById($user_id);
								 $user_lat=$userDetailsLog['user_lat'];
							     $user_long=$userDetailsLog['user_long'];
								//------------------------------------------------------------
								foreach($data as $userDatas)
						        {						
									
										 $user_list_id= $userDatas['user_id'];
										 $likeStatus=$this->getUserLikeStatus($user_id,$user_list_id);
										 
										 if($likeStatus==1 || !$likeStatus){
											$userDetails=getUserDetailsById($user_list_id);
											// $respData['is_vip']=$userDetails['is_vip'];
											$mutualStatus=$this->checkMutualStatus($user_id,$user_list_id); 
											$respData['isMutual']=$mutualStatus;
											
											$respData['user_list_id']=$user_list_id;
											$respData['user_fullname']=$userDetails['user_fullname'];
											$respData['is_online']=$userDetails['is_online'];
											$respData['user_profile_pic']=$userDetails['user_profile_pic'];
											$respData['user_email']=$userDetails['user_email'];
											$AllUsers[]= $respData;
										}
								
								if($token===10){break;}else{ $token++;}	
								$j++;
							  }
								if($arraycount>=10){
										$AllUsers['list_token']=$data[$j]['createdOn'];
									 }else{
										$AllUsers['list_token']=$data[$arraycount-1]['createdOn']; 
									 }
									 	
						     	
							}else{
								$AllUsers['list_token']=date('Y-m-d H:i:s',time());
							}
					        return $AllUsers;
						 //--------------------------end of token not 0------------------------------------------------------
						
					}
		}	
}

   //--------------------------show near by rang hangout----------------------------------------------
  
   public function getHangoutByRang($rang,$user_lat,$user_long,$user_id)
   {
   
	    $token=1;
		$i=0;
		$j=0;
    	
 
        $select=array('tbl_hangout.user_id','hangout_id','hangout_title','hangout_description','media_url','media_type','check_in'
					  ,'tbl_hangout.created_on','user_fullname','user_email','user_lat','user_long','tbl_hangout.tag_id');
        $this->db->select($select);
		$where=array('user_status'=>1);
        $this->db->where($where);
        $this->db->from('tbl_hangout');
		$this->db->order_by("tbl_hangout.created_on","desc");
        $this->db->join('tbl_users','tbl_users.user_id=tbl_hangout.user_id');
        $datas = $this->db->get();
        $data= $datas->result_array(); 
        //echo $this->db->last_query();
        //echo '<pre>'; print_r($data);die;
        $arraycount=count($data);
        $respData= array();
        $AllUsers= array();
        if(!empty($data))
		{
			
			foreach($data as $userDatas)
	        {						
		    	 $theta = $user_long - $userDatas['user_long'];
				 $dist = sin(deg2rad($user_lat)) * sin(deg2rad($userDatas['user_lat'])) +  cos(deg2rad($user_lat)) * cos(deg2rad($userDatas['user_lat'])) * cos(deg2rad($theta));
				 $dist = acos($dist);
				 $dist = rad2deg($dist);
				 $miles = $dist * 60 * 1.1515;
				  if($miles<=$rang)
				    {
						$user_to_id= $userDatas['user_id'];
					    $hangout_id= $userDatas['hangout_id'];
						
						$whereReport=array('user_id'=>$user_id,'hangout_id'=>$hangout_id);
						$reportStatus=$this->select('tbl_hangout_report','*','s','c',$whereReport,false,false,false,false,false);
						
						if($reportStatus<=0 || $reportStatus==""){
							
							$hangoutdetail=$this->getHangOutDetailByID($user_to_id,$hangout_id);
					  //  print_r($hangoutdetail);
						if($hangoutdetail){
							
								///show hangout of last 4 days
								$respData['hangout_id']=$hangout_id;
								$respData['hangout_title']=$hangoutdetail[0]['hangout_title'];
								$respData['hangout_i_liked']=$this->checkILikedHangout($user_id,$hangout_id);
								$respData['hangout_description']=$hangoutdetail[0]['hangout_description'];
								$respData['user_to_id']=$user_to_id;
								$userDetails=getUserDetailsById($user_to_id);
								$respData['user_fullname']=$userDetails['user_fullname'];
								$respData['user_city']=$userDetails['user_city'];
								$respData['user_profile_pic']=$userDetails['user_profile_pic'];
								$respData['hangout_status']=$hangoutdetail[0]['hangout_status'];
								if($hangoutdetail[0]['media_url']){
								$respData['media_url']=$hangoutdetail[0]['media_url'];
								}else{
								$respData['media_url']="";//http://shapingtechnology.com/demo/skout/api_assets/images/avtar.png	
								}
								$respData['media_type']=$hangoutdetail[0]['media_type'];
								$respData['check_in']=$hangoutdetail[0]['check_in'];							
								$respData['hangout_likes']=$this->getTotalHangoutLikesById($hangout_id);
								$respData['hangout_comments']=$this->getTotalHangoutCommentsById($hangout_id);
								$respData['created_on']=$hangoutdetail[0]['created_on'];
								
								if($hangoutdetail[0]['tag_id']!=0){
									$tagId = $hangoutdetail[0]['tag_id'];
									}else{
										 $tagId='';
									}
								$respData['tag_id']=$tagId;
								$respData['tag_name']=getTagNameById($hangoutdetail[0]['tag_id']);
								
								
								
								//if(1<=$interval->d  &&   '0' !=$interval->d ){
								
								//$respData['created_hangout_time']=	$interval->d."d";
								//}
								//else if(23>=$interval->h && '0' !=$interval->h  )
								//{
									
									//$respData['created_hangout_time']=	$interval->h."h";
								//}
								//else if(59>=$interval->i){
									
									//$respData['created_hangout_time']=	$interval->i."min";
								//}
								date_default_timezone_set("Asia/Calcutta");
								 $current_date=date('Y-m-d H:i:s',time()); 
								 $next_date=$respData['created_on'];
								
								$date_a = new DateTime($next_date);
								$date_b = new DateTime($current_date);
								$diff34 = date_diff($date_a,$date_b);
								
								
								//accesing days
								 $days = $diff34->d; 
								//accesing months
								 $months = $diff34->m; 
								//accesing years
								 $years = $diff34->y; 
								//accesing hours
								 $hours=$diff34->h; 
								//accesing minutes
								 $minutes=$diff34->i; 
								//accesing seconds
								 $seconds=$diff34->s;
								
								//echo $interval_time=$days.':'.$months.':'.$years.':'.$hours.':'.$minutes.':'.$seconds;
								//die; 
								if($minutes>1 && $hours<1 && $days<1){
								
								$respData['created_hangout_time']=$minutes .' min';
								}
								else if($seconds>0 && $minutes<1 && $hours<1)
								{
									$respData['created_hangout_time']=$seconds .' sec';
								}
							    else if($hours>1 && $seconds>=0 && $minutes>=0){
									
									$respData['created_hangout_time']=	$hours .' hr';
								}
								else if($days>0 && $hours>=0 && $seconds>=0 && $minutes>=0){
									
									$respData['created_hangout_time']=	$days .' days';
								}else{
									
									$respData['created_hangout_time']=	0 .' day';
								}
								
								
								//--------------------------------------------
								
								
								
								$AllUsers[]= $respData;	
						 
						 
						 }
						
						
						
						
							
						}
					}
					
			}
		}
		else{
			//echo "Randome date";
			return FALSE;
		}
		//print_r($AllUsers);die;
		   return $AllUsers;
        //--------------------------end of token  0------------------------------------------------------
		
   }

   
   //---------------------------------Add Hangout Likes-----------------------------------------------
   public function addLikes($user_id,$hangout_id)
   {
	   
	   $current_time=date('Y-m-d H:i:s',time());
	   $this->db->select('*');
	   $this->db->from('tbl_hangout_like');   
	   $where=array('user_id'=>$user_id,'hangout_id'=>$hangout_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {	   
		   return 'alerady_liked';
	   }
	   else
	   { 
		  $array=array('user_id'=>$user_id,'hangout_id'=>$hangout_id,'hangout_like	'=>1,'created_on'=>$current_time);
		  $hangout_like_id= $this->insert_query('tbl_hangout_like',$array);
		  if($hangout_like_id)
		  {
			  return 'liked';
		  }else{
			  return 'server_error';
		  }
		  
	   }
   }
   /// -------------------------------------------get hangout comment list------------------------------------
   public function getHangoutCommentList($hangout_id)
   {
	    $token=1;
		$i=0;
		$j=0;
    	
				$select=array('user_id','comment','created_on');
				$this->db->select($select);
				//$where=array('tbl_hangout.createdOn >'=>$list_token);
				$this->db->from('tbl_hangout_comment');
				$this->db->where('hangout_id',$hangout_id);
				$datas = $this->db->get();
				$data= $datas->result_array(); 
				//echo '<pre>'; print_r($data);die;
				$arraycount=count($data);
				$respData= array();
				$AllUsers= array();
				if(!empty($data))
				{
					foreach($data as $userDatas)
					{	
						$user_id=$userDatas['user_id'];
						$userDetails=getUserDetailsById($user_id);
						$respData['user_fullname']=$userDetails['user_fullname'];
						$respData['user_profile_pic']=$userDetails['user_profile_pic'];
						$respData['comment']=$userDatas['comment'];
						$respData['created_on']=$userDatas['created_on'];
						$AllUsers[]=$respData;
						
					}
					//print_r($AllUsers);die;
					return $AllUsers;
				}else
				{
					return false;
				}
				
		
   }
   /// -------------------------------------------Emd hangout comment list------------------------------------
   
   //---------------------------------End Hangout Likes-----------------------------------------------
   
   public function getHangOutDetailByID($user_id,$hangout_id)
   {
	   $this->db->select('*');
	   $this->db->from('tbl_hangout');
	   $where=array('user_id'=>$user_id,'hangout_id'=>$hangout_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
		   return $query->result_array();
	   }
	   else
	   {
		   return false;
	   }
   }
   
   //-----Get Hangout likes
   public function getTotalHangoutLikesById($hangout_id)
   {
	   $this->db->select('*');
	   $this->db->from('tbl_hangout_like');   
	   $where=array('hangout_like'=>1,'hangout_id'=>$hangout_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
		   $countLikes= (string)$query->num_rows();
		   return $countLikes;
	   }
	   else
	   {
		   $countLikes="0";
		   return $countLikes;
	   }
   }
   ///Check I Liked hangout or not
   public function checkILikedHangout($user_id,$hangout_id)
   {
	   $this->db->select('*');
	   $this->db->from('tbl_hangout_like');   
	   $where=array('hangout_like'=>1,'hangout_id'=>$hangout_id,'user_id'=>$user_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   //echo $this->db->last_query();echo "<br>";
	   if($query->num_rows()>0)
	   {
		   $countLikes= (string)$query->num_rows();
		   return $countLikes;
	   }
	   else
	   {
		   $countLikes="0";
		   return $countLikes;
	   }
   }
   ////Check Like count Of the Day
	public function checkLikeCountOfDay($user_id,$hangout_id)
	{
	   echo $current_time=date('Y-m-d');
	   $this->db->select('*');
	   $this->db->from('tbl_hangout_like'); 
	   $this->db->like('created_on',$current_time);   
	   $where=array('user_id'=>$user_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   echo $this->db->last_query();echo "<br>";
	   if($query->num_rows()>0)
	   {
		   $countLikes= (string)$query->num_rows();
		   echo  $countLikes;die;
	   }
	   else
	   {
		  echo  $countLikes="0";die;
		   return $countLikes;
	   }
	}
   ///Get Total hangout comment
    public function getTotalHangoutCommentsById($hangout_id)
   {
	   $this->db->select('*');
	   $this->db->from('tbl_hangout_comment');   
	   $where=array('status'=>1,'hangout_id'=>$hangout_id);
	   $this->db->where($where);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
		   $countLikes= (string)$query->num_rows();
		   return $countLikes;
	   }
	   else
	   {
		   $countLikes="0";
		   return $countLikes;
	   }
   }
   
    public function checkMutualStatus($user_id,$user_to_id)
   {
   //----check for is i like him-------------------------------------------------------------------------------------------------	
			$this->db->select('like_status');
			$where1=array('user_id'=>$user_id,'user_to_id'=>$user_to_id,);// is i like him
			$this->db->where($where1);
			$this->db->from('tbl_user_like');
			$dataM = $this->db->get();
			$like_status1= $dataM->row()->like_status; 
			  //if(empty($like_status1)){
			 //	return '0';
		    //}
		   //echo $this->db->last_query(); echo '<br>';echo '<br>';
          //----check for he like me------------------------------------------------------------------
			$this->db->select('like_status');
			$where2=array('user_id'=>$user_to_id,'user_to_id'=>$user_id);// is he like me
			$this->db->where($where2);
			$this->db->from('tbl_user_like');
			$dataM1 = $this->db->get();
			if(empty($dataM1)){
			$like_status2= $dataM1->row()->like_status; 
			
		    	//if(empty($like_status2)){
			   //	return '0';
			  //}
			 //echo $this->db->last_query(); echo '<br>';echo '<br>';
		if($like_status1==$like_status2){
	        	return '1';
		}else {
				return '0';
			}
		}else{ 
				return '0';
			}
	}
	
	
   public function addNotification($user_id_to,$user_id_from,$noti_txt)
   {
       $created_on  =date('Y-m-d H:i:s');
	   $noti_array=array('user_id_to'=>$user_id_to,
	   					 'user_id_from'=>$user_id_from,
	   					 'noti_txt'=>$noti_txt,
	   					 //'noti_txt_nonvip'=>$noti_txt_nonvip,
	   					 'noti_status'=>1,
	   					 'createdOn'=>$created_on
						 );
		$insertNotification=$this->insert_query('tbl_notification',$noti_array);				 
    	if($insertNotification>0){
    		return true;
    	}else{
    		return false;
    	}
		  
   }
   public function select_userlist_limit($limit, $start) 
		{
        $this->db->limit($limit, $start);
        $query = $this->db->get("tbl_users");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            $array = $data;
           // print_r($array);die;
            return $array;
        }
        else
        {
        return false;
	    }
        }
    public function select_hangoutlist_limit($limit, $start) 
		{
        $this->db->limit($limit, $start);
        //$this->db->join('tbl_users', 'tbl_users.user_id=tbl_hangout.user_id');
        $query = $this->db->get("tbl_hangout");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            $array = $data;
           // print_r($array);die;
            return $array;
        }
        else
        {
        return false;
	    }
        }
        public function select_giftlist_limit($limit, $start) 
		{
        $this->db->limit($limit, $start);
		$this->db->order_by("gift_id", "desc");
        $query = $this->db->get("tbl_gifts");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            $array = $data;
           // print_r($array);die;
            return $array;
        }
        else
        {
        return false;
	    }
        }
         public function select_stickerlist_limit($limit, $start) 
		{
        $this->db->limit($limit, $start);
        $query = $this->db->get("tbl_sticker");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            $array = $data;
           // print_r($array);die;
            return $array;
        }
        else
        {
        return false;
	    }
        }
        
        public function LikeCount($user_id) 
		{
			$this->db->select("*");
			$this->db->from('daily_like_count');
			$this->db->where('user_id',$user_id);
			$this->db->where('date',date('Y-m-d',time()));
			$query = $this->db->get();// execute
		   //echo $this->db->last_query();die;
      		if($query->num_rows()>0)
		   {
			return $query->row_array();// select All value from table
			//return $query->row;// select single row from table
			//return true;
		   }
		  else
		   {
			return false;
		   }
		}
		
		
		// get total like of user
		public function TotalLike($tbl,$where) 
		{
			$this->db->select('count(user_id) as `total`');
			$this->db->from($tbl);
			$this->db->where($where);
				$query = $this->db->get();// execute
			//echo $this->db->last_query();die;
			if($query->num_rows()>0)
			{
				return $query->row_array();// select All value from table
				//return $query->row;// select single row from table
				//return true;
			}
			else
			{
				return false;
			}
		}
		public function getTotalLikeUser($hangout_id) 
		{ 
			
			$this->db->select('tbl_hangout_like.user_id,tbl_users.user_fullname');
			$this->db->from('tbl_hangout_like');
			$this->db->where(array('tbl_hangout_like.hangout_like'=>1,'hangout_id'=>$hangout_id));
			$this->db->join('tbl_users','tbl_users.user_id = tbl_hangout_like.user_id');

			
			
			
			$query = $this->db->get();// execute
			//print_r($query);die;
			//echo $this->db->last_query();die;
			if($query->num_rows()>0)
			{
				$res=$query->result_array();
				//echo $this->db->last_query();
				//print_r($res);die;
				return $res;// select All value from table
				//return $query->row;// select single row from table
				//return true;
			}
			else
			{
				return false;
			}
		}
		
		public function deleteHangoutRecord($finalIds) 
		{
			$sql1="DELETE FROM `tbl_hangout` WHERE `hangout_id` IN ($finalIds)";
			$sql2="DELETE FROM `tbl_hangout_comment` WHERE `hangout_id` IN ($finalIds)";
			$sql3="DELETE FROM `tbl_hangout_like` WHERE `hangout_id` IN ($finalIds)";	
			
			
			$myquery1 = $this->db->query($sql1);
			$myquery2 = $this->db->query($sql2);
			$myquery3 = $this->db->query($sql3);
		}
		
		public function selectGiftCatIds() 
		{ 
			
			$this->db->select('gift_category_id');
			$this->db->distinct();
			$query = $this->db->get('tbl_gifts');// execute
			//print_r($query);die;
			//echo $this->db->last_query();die;
			if($query->num_rows()>0)
			{
				$res=$query->result_array();
				return $res;
			}
			else
			{
				return false;
			}
		}
		
		public function selectGifts($gift_category_id) 
		{ 
			$blankArray=array();
			$data=array();
			$res=array();
			$result=array();
			$this->db->select('*');
			$this->db->from('tbl_gifts');
			$this->db->distinct();
			$this->db->where_in('gift_category_id',$gift_category_id);
			$datas = $this->db->get();
			$data= $datas->result_array(); 
			
			//echo $this->db->last_query();die;
			/*
			[gift_id] => 1
            [gift_image] => http://shapingtechnology.com/demo/skout/api_assets/gifts/6340.jpg
            [coins] => 30
            [gift_category_id] => 4
            [gift_status] => 1
            [createdOn] => 2016-04-20 12:51:18
			*/
			foreach ($data as $value) {
				$res['gift_id']=$value['gift_id'];
	            $res['gift_image']=$value['gift_image'];
	            $res['coins']=$value['coins'];
	            $res['gift_category_id']=$value['gift_category_id'];
	            $res['gift_status']=$value['gift_status'];
				$result['block'][]=$res;
			}
			
			//echo '<pre>';print_r($result);die;
			
			$a=explode(',',$gift_category_id);
			//print_r($a);die;
			$result['gift_category_name']=getgiftcategoryNameById($a[0]);
			if(!empty($result))
			{
				return $result;
			}
			else
			{
				return $blankArray;
			}
		}
		
		public function getTotalLikeUsers($user_id) 
		{ 
			
			$this->db->select('tbl_user_like.user_id,tbl_users.user_fullname');
			$this->db->from('tbl_user_like');
			//$this->db->where(array('tbl_user_like.like_status'=>1,'user_id'=>$user_id));
			$this->db->where(array('tbl_user_like.like_status'=>1,'tbl_user_like.user_id'=>$user_id));
			$this->db->join('tbl_users','tbl_users.user_id = tbl_user_like.user_id');
            
			$query = $this->db->get();// execute
			//echo $this->db->last_query();die;
			if($query->num_rows()>0)
			{
				$res=$query->result_array();
				return $res;// select All value from table
				//return $query->row;// select single row from table
				//return true;
			 }else{
				return false;
			 }
		}
	
	 //update coins by user id
	 public function updateCoinsByUserId($user_id,$coins){
			 $this->db->set('coins','coins+'.$coins, FALSE);
			 $this->db->where('user_id',$user_id);
		     if($this->db->update('tbl_users'))
		     {
		       	 return $this->db->affected_rows();
		     }else{
					return false;
			 } 	
		}

 public function subtractCoinsForGiftByUserId($user_id){
			 $this->db->set('coins','coins-2', FALSE);
			 $this->db->where('user_id',$user_id);
		     if($this->db->update('tbl_users'))
		     {
		       	 return $this->db->affected_rows();
		     }else{
					return false;
			 } 	
		}
 
 
 //rise up user account
	 public function riseUpUserAccount($user_id,$require_coins){
	 	
	 	     $current_time=date('Y-m-d H:i:s',time());
			 $this->db->set('coins','coins-'.$require_coins, FALSE);
			 $this->db->set('spend_coins','spend_coins+'.$require_coins, FALSE);
			 $this->db->where('user_id',$user_id);
			  
		     if($this->db->update('tbl_users'))
		     {
				return true;
		     }else{
					return false;
			 } 	
		}
	 
 //rise up request by user
	 public function riseUpRequestByUser($user_id){
	 	
	 	     $current_time=date('Y-m-d H:i:s',time());
			 $this->db->set('coins','coins-5', FALSE);
			 $this->db->set('spend_coins','coins+5', FALSE);
			 $this->db->where('user_id',$user_id);
			  
		     if($this->db->update('tbl_users'))
		     {
				return true;
		     }else{
					return false;
			 } 	
		}	 

 //add user as vip
	 public function addVipUser($user_id,$coins,$plan_type,$days){
	 	
	 	     $current_time=date('Y-m-d H:i:s',time());
			 $this->db->set('coins','coins-'.$coins, FALSE);
			 $this->db->set('is_vip','1');
			 $this->db->where('user_id',$user_id);
			 
			 $dt = date("Y-m-d H:i:s");
             $end_date= date( "Y-m-d H:i:s", strtotime( "$dt +$days day" ) );
			 
		     if($this->db->update('tbl_users'))
		     {
		     	//$updateCoins=$this->updateCoinsByUserId($user_id,$coins);
				
		     	$insertArray=array('user_id'=>$user_id,'vip_user_created_on'=>$current_time,'vip_user_end_date'=>$end_date,'vip_plan_id'=>$plan_type);
		       	$insert=$this->insert_query('tbl_vip_user_details',$insertArray);
				return $insert;
		     }else{
					return false;
			 } 	
		}
	 //update user as vip 
	 
	 public function updateVipUser($user_id,$coins,$plan_type,$days){
	 	
	 	     $current_time=date('Y-m-d H:i:s',time());
			 $this->db->set('coins','coins-'.$coins, FALSE);
			  
			  $this->db->set('is_vip','1');
			 $this->db->where('user_id',$user_id);
			 
			 $dt = date("Y-m-d H:i:s");
             $end_date= date( "Y-m-d H:i:s", strtotime( "$dt +$days day" ) );
			 
		     if($this->db->update('tbl_users'))
		     {
		     	//$updateCoins=$this->updateCoinsByUserId($user_id,$coins);
				
		     	$up_data=array('vip_user_created_on'=>$current_time,'vip_user_end_date'=>$end_date,'vip_plan_id'=>$plan_type);
		       	$where=array('user_id'=>$user_id);
		       	$updateData=$this->update_query('tbl_vip_user_details',$up_data, $where);
				return $updateData;
		     }else{
					return false;
			 } 	
		}
	
	 /*** Forget user password ***/
	function forgetuserpassword($email_value)
	{
		//echo $email_value;die;
		$cdetae=date('Y-m-d H:i:s');
		$this->update_query('user',array('forget_timestamp'=>$cdetae),array('email'=>$email));
		$this->db->where('email',$email_value);
		$query = $this->db->get('user');
		//print_r($query);
		//echo $query->num_rows;
		//die;
		if($query->num_rows()>0)
		{
			//echo "text";die;
			 $pass= random_string('alnum',8);
		    //print_r($pass);die;
			$pwd=$pass;
			//print_r($pwd);die;
			$user_data = array(
		    		'password'=>	$pwd
				);
				//print_r($user_data);die;
		 
			$this->db->where('email',$email_value);
			$this->db->update('user',$user_data); 
			
			$query1 = $query->result();
			//print_r($query1);die;
	
			$data = array(
					'user_id' 		=> $query1[0]->user_id,
					'email' 		=> $query1[0]->email,
					'firstname' 		=> $query1[0]->firstname
					
				);
				//print_r($data);die;
			$password = $this->generateRandomPassword(); 
			//print_r($password);die;	
			$this->send_new_password_email($data['user_id'], $data['email'],$data['firstname'],$pass);
			return true;
		}
		else
		{
			return false;
		}
	}
	 //*** send_password_to user***/
	
	function send_new_password_email($user_id,$user_email,$user_fullname,$pass)
	{
		//echo $admin_email;die;
		$this->load->library('parser');
		$data = array('firstname' =>$user_fullname,'password' =>$pass,'email'=>$user_email);
		$html=$this->parser->parse('forgetnew_passwordTemp', $data);
		
		$result = '';
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->set_mailtype("html");

		$this->email->from('http://freebizoffer.com', 'Pickanddeliver'); 
		$this->email->to($user_email);

		$this->email->subject('password update');

		$this->email->message($html);

		if ($this->email->send())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
		

public function verifyemailAddress($email)
	{
    $check=$this->db->get_where('user',array('email'=>$email));
	$count=$check->num_rows();  

	    $res= $check->row();
	    $data = new stdClass();
	    $data->email    = $res->email;
	    $data->firstname = $res->firstname;       
	 
	    $subject = "Verify Your Email Address";
	    $message = $this->load->view('verifyemailTemp',$data,true);
	    $config=array(
	    'charset'=>'utf-8',
	    'wordwrap'=> TRUE,
	    'mailtype' => 'html'
	    );
        $this->load->library('email');
	    $this->email->initialize($config);
	    $this->email->set_newline("\r\n");
	    $this->email->from('freebizoffer.com','Qpeds'); 
	    $this->email->to($email);
	    $this->email->subject($subject);
	    $this->email->message($message);
	 
	    if($this->email->send()){  
		     // echo "sending";
		      return 1;
	    }else{
	    //	echo $this->email->print_debugger();
	    //echo "notsend"  ;
		      return 0;  
	    }
	}		
	/*** send forgetpassword  for admin***/
	
	function send_forgetpassword_email($admin_id, $admin_email,$admin_password)
	{
		//echo $admin_email;die;
		$result = '';
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->set_mailtype("html");

		$this->email->from('http://freebizoffer.com', 'Qpeds'); 
		$this->email->to($admin_email);

		$this->email->subject('Forget password');

		$this->email->message('Hello '.$admin_email.',<br><br>  Thank you for reset your account password. <br> Please Login http://localhost/superadmin and update your password for more Security.<br> Password: '.$admin_password.'<br><br>Thank you!');

		if ($this->email->send())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*** for generate Random Password ***/
	function generateRandomPassword()
	{
	  $password = '';
	
	  $desired_length = rand(8, 20);
	  //print_r($desired_length);die;
	  for($length = 0; $length < $desired_length; $length++)
	  {
	  	$password .= chr(rand(32, 126));
	  }
	  return $password;
	}
	
	/*** Forget password ***/
	function forgetpassword($email_value)
	{
		//echo $email_value;die;
		
		$this->db->where('admin_email',$email_value);
		$query = $this->db->get('tbl_admin');
		//print_r($query);die;
		//echo $query->num_rows;
		
		if($query->num_rows()>0)
		{
			//echo "text";die;
			 $pass= random_string('alnum',8);
		    //print_r($pass);die;
			$pwd=md5($pass);
			//print_r($pwd);die;
			$user_data = array(
		    		'admin_password'=>	$pwd
				);
				//print_r($user_data);die;
		 
			$this->db->where('admin_email',$email_value);
			$this->db->update('tbl_admin',$user_data); 
			
			$query1 = $query->result();
			//print_r($query1);die;
	
			$data = array(
					'admin_id' 		=> $query1[0]->admin_id,
					'admin_email' 		=> $query1[0]->admin_email
					
				);
				//print_r($data);die;
			$password = $this->generateRandomPassword(); 
			//print_r($password);die;	
			$this->send_forgetpassword_email($data['admin_id'], $data['admin_email'],$pass);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	
	function unlinkFileByFolderNameAndFileName($folderPath,$fileName)
	{
		//$folderPath,$fileName
		//$folderPath='/api_assets/images/';
		//$fileName='6548.jpg';
		$m_img_real=$_SERVER['DOCUMENT_ROOT'].$folderPath.$fileName;
		
		
		if (file_exists($m_img_real)) 
		{
		     unlink($m_img_real);
		     return 'deleted';
		}else{
			 return 'file not exists';
		}

	}
	function getLikedUsersDetails($res_arr)
	{
		$this->db->select("*");
		$this->db->from("tbl_users");
		$this->db->where_in('user_id',$res_arr);
		$query =  $this->db->get();
		return $query->result_array();
		
		
	}
	function getLikedHangoutsDetails($res_arr)
	{
		$this->db->select("*");
		$this->db->from("tbl_hangout");
		$this->db->where_in('hangout_id',$res_arr);
		$query =  $this->db->get();
		//print_r($query);die;
		return $query->result_array();
	}

	
	
}

?>
