<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');
ob_start();

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Authentication');
    //    $this->load->library('session');
    }

    public function index() {
        //echo "dfsf";die;
        //var_dump($this->session->userdata('logged_in'));die;
      
       /*if ($this->session->userdata('logged_in')) {
            redirect('Admin/userlist');
        }*/
        if (!empty($_POST)) {
             $email = $this->input->post('email');
             $pass = $this->input->post('password');
            
             $where_arr = array('email'=>$email,'password'=>$pass,'type'=>1);
             //$res = $this->Authentication->select_rowwhere_query('user',$where_arr);
             $res = $this->Authentication->select_rowwhere_query('user',$where_arr);
              //print_r($res);die;
              if ($res > 0) {
                    $userdata = array('user_id' => $res->user_id,'name' => $res->firstname.' '.$res->lastname, 'email' => $res->email,'admin_type' => $res->admin_type, 'logged_in' => TRUE);
                    //$user_id = $res->user_id;
                    $this->session->set_userdata($userdata);
                    //print_r($this->session->userdata('email'));die;
                    redirect('Admin/userlist');
            
                } else {
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Error! Invalid Credentials</div>');
                $this->load->view('login');
            }
        } else {
          // echo "test1";die;
            $this->load->view('login');
        }
    }
public function userlist() {
       //echo $this->session->userdata('user_id');die;
    //if(!empty($this->session->userdata('user_id'))!=''){
        //print_r($id);die;
       //if($this->session->userdata('user_id')!=''){ 
        $this->load->view('header');
        $data = $this->Authentication->select_data('user', array('type' => 0, 'user_type' => 1));
        //echo '<pre>';print_r($data);die();
        $data = array('users' => $data);
        $this->load->view('userlist', $data);
      // }else{
     //   echo "dfd";die;
     //   $this->load->view('login');
       //}
//        $this->load->view('footer');
    }

    public function signup() {
        if ($this->session->userdata('logged_in')) {
            //redirect('admin/dashboard');
        }

        $this->load->view('signup');
    }

    public function register() {
        $post = $this->input->post();

        if ($this->Authentication->registernewuser($post)) {

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! You have registered successfully. Please login to continue</div>');
            return redirect('auth/index');
        }
    }

    public function checkemail() {
        $email = $this->input->post('email');
        $data = $this->Authentication->checkemail($email);
        if ($data) {
            echo $email;
        } else {
            
        }
    }

    public function sendmessage($mobile) {
        $OTP = $this->generateRandomStringOTP();
        $body = "OTP For Beacon Hustle Login :" . $OTP;
        $number = '+91' . $mobile;
        $_SESSION["OTP"] = $OTP;
        //$code='testing';
        $code = $body;


        //$sid = "AC276498af0b6dbc0948dd1ab0d4d279e1"; // Your Account SID from www.twilio.com/user/account
        $sid = "AC05d1431bbd0677fc961f9f7f384cd5ba"; // Your Account SID from www.twilio.com/user/account
        $token = "cbcc8071effa501094458dadcccf537f"; // Your Auth Token from www.twilio.com/user/account
        //$token = "45574c16ec4866b31ea1067775faee98"; // Your Auth Token from www.twilio.com/user/account

        $client = new Services_Twilio($sid, $token);
        $message = $client->account->messages->sendMessage(
                //'+6 596-512-729', // From a valid Twilio number
                '+1 201-897-7191', // From a valid Twilio number
                $number, // Text this number
                $code
        );

        echo $message->sid;
    }

    public function generateRandomStringOTP($length = 6) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function verification() {
        $this->load->view('verification');
    }

    public function mobilveryfy() {
        $otp = $this->input->post('otp');
        $data = $this->Authentication->checkmobile($otp);
        if ($data) {
            if ($data->status == "1" || $data->status == "3") {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Error! Your mobile number XXXXXXXX' . substr($data->phone_no, -2) . ' is already verified. <a href="' . base_url('auth') . '">Click</a> here to login.</div>');
                return redirect('auth/verification?status=alreadyverified&otp=' . $otp . '');
            }
            $time = strtotime($data->reg_date);
            $newformat = date('Y-m-d H:i:s', $time);
            $currenttime = date('Y-m-d H:i:s');
            //echo $currenttime; die;
            $datetime1 = new DateTime($newformat);
            $datetime2 = new DateTime($currenttime);
            $interval = $datetime1->diff($datetime2);
            $intervalmin = $interval->format('%i');
            $intervalhour = $interval->format('%h');
            $intervalday = $interval->format('%a');
            if ($intervalmin > 30 || $intervalhour > 0 || $intervalday > 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Error! Mobile verification code has been expired.<a href="' . base_url('auth/resendotp') . '/' . $otp . '">click</a> here to resend verification code.</div>');
                return redirect('auth/verification?status=expired&otp=' . $otp . '');
            } else {
                if ($data->status == "0") {
                    $this->Authentication->mobileverified(1, $data->id);
                } else {
                    $this->Authentication->mobileverified(3, $data->id);
                }
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
               Success! Your mobile number XXXXXXXX' . substr($data->phone_no, -2) . ' verified sucessfully. Please login.</div>');
                return redirect('auth');
            }
        } else {
            return redirect('auth/verification');
        }
    }

    public function resendotp($otp) {
        $newotp = $this->generateRandomStringOTP();
        $data = $this->Authentication->checkmobile($otp);
        if (!$data) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Access Denied! Page you have requested is not granted.</div>');
            return redirect('auth/unauthaccess');
        }
        //echo $newotp; die;
        if ($this->Authentication->resendotp($otp, $newotp)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Your mobile verification code is send to your mobile number XXXXXXXX' . substr($data->phone_number, -2) . '</div>');

            return redirect('auth/verification?status=resendotp&otp=' . $otp . '');
        }
    }

    public function logincheck() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = $this->Authentication->logincheck($email, $password);
        if ($data) {
            if ($data->status == "2" || $data->status == "3") {
                $this->session->set_userdata($data);
                return redirect('admin/index');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Your email account is not verified yet. Please follow the verification link send to your email id ' . substr($data->email, 0, 3) . '*********' . substr($data->email, -10) . '. </div>');
                return redirect('auth/index?status=authfailed');
            }
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Invalid email or password.</div>');
            return redirect('auth/index?status=authfailed');
        }
    }

    public function mailveryfy($otp) {

        $data = $this->Authentication->checkmobile($otp);
        if ($data) {
            if ($data->status == "2" || $data->status == "3") {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Your email ' . substr($data->email, 0, 3) . '*********' . substr($data->email, -10) . ' is already verified. <a href="' . base_url('auth') . '">click</a> here to login.</div>');
                return redirect('auth/emailverification?status=alreadyverified&otp=' . $otp . '');
            }
            $time = strtotime($data->reg_date);
            $newformat = date('Y-m-d H:i:s', $time);
            $currenttime = date('Y-m-d H:i:s');
            //echo $currenttime; die;
            $datetime1 = new DateTime($newformat);
            $datetime2 = new DateTime($currenttime);
            $interval = $datetime1->diff($datetime2);
            $intervalmin = $interval->format('%i');
            $intervalhour = $interval->format('%h');
            $intervalday = $interval->format('%a');

            // echo 'yes';die;
            if ($intervalmin > 30 || $intervalhour > 0 || $intervalday > 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Your email verification link has been expired.<a href="' . base_url('auth/resendmail') . '/' . $otp . '">Click</a> here to resend verification link.</div>');

                return redirect('auth/emailverification?status=expired&otp=' . $otp . '');
            } else {
                if ($data->status == "0") {
                    $this->Authentication->mobileverified(2, $data->id);
                } else {
                    $this->Authentication->mobileverified(3, $data->id);
                }
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Your email verification successfully completed. Please login.</div>');
                return redirect('auth');
            }
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Access Denied! Request page is not granted access.</div>');
            return redirect('auth/unauthaccess');

            // return redirect('auth/emailverification?status=notmatched');
        }
    }

    public function resendmail($otp) {
        $newotp = $this->generateRandomStringOTP();
        $data = $this->Authentication->checkmobile($otp);
        //echo $newotp; die;
        if ($this->Authentication->resendotp($otp, $newotp)) {
            $body = '<a href="' . base_url('auth/mailveryfy') . '/' . $newotp . '">Click here </a>to verify your email.<br>';
            $body.='or paste this url manually to on the browser ' . base_url('auth/mailveryfy') . '/' . $newotp;
            $body.='<br>Link has been expired in 30 minuts.';
            Send_Mail($data->email, 'canopus.testing@gmail.com', $body, "Your new verification link.");
            return redirect('auth/emailverification?status=resendotp&otp=' . $otp . '');
        }
    }

    public function emailverification() {
        $this->load->view('emailverification');
    }

    public function forgetpassword() {
        $this->load->view('forgetpassword');
    }

    public function forgetpasswordlink() {

        $email = $this->input->post('email');


        if ($email) {

            //$body = '<a href="' . base_url('auth/resetpassword') . '/' . $newotp . '">Click here </a>to verify your email.          <br>';
            //$body.='or paste this url manually to on the browser ' . base_url('auth/resetpassword') . '/' . $newotp;
            //$body.='<br>Link has been expired in 30 minuts.';
            $body = 'Hi, <br/> <br/> Here is password reset link: <br/> <br/> <a href="' . base_url('auth/resetpassword') . '">' . base_url('auth/resetpassword') . '</a><br/> <br/>';
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From:demo@canopusinfosystems.com' . "\r\n";
            mail($email, 'Forgot Password', $body, $headers);

            // Send_Mail($data->email, 'canopus.testing@gmail.com', $body, "Forget Password.");
            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Password reset link send to ' . $email . '. </div>');
            return redirect('auth/forgetpassword');
        } else {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! ' . $email . ' not register with us. </div>');
            return redirect('auth/forgetpassword');
        }
        $this->load->view('forgetpassword');
    }

    public function resetpassword($newotp) {
        $data = $this->Authentication->checkmobile($newotp);
        if ($data) {
            $time = strtotime($data->reg_date);
            $newformat = date('Y-m-d H:i:s', $time);
            $currenttime = date('Y-m-d H:i:s');
            //echo $currenttime; die;
            $datetime1 = new DateTime($newformat);
            $datetime2 = new DateTime($currenttime);
            $interval = $datetime1->diff($datetime2);
            $intervalmin = $interval->format('%i');
            $intervalhour = $interval->format('%h');
            $intervalday = $interval->format('%a');
            if ($intervalmin > 30 || $intervalhour > 0 || $intervalday > 0) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Access Denied! Link has been expired.</div>');
                return redirect('auth/unauthaccess');
            }

            $data = array('userdata' => $data);
            $this->load->view('resetpassword', $data);
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Access Denied! Request page is not granted access.</div>');
            return redirect('auth/unauthaccess');
        }
    }

    public function unauthaccess() {
        $this->load->view('unauthaccess');
    }

    public function changepassword() {
//var_dump($_POST);die;
        $id = $this->input->post('id');
        $otp = $this->input->post('otp');
        $pwd = $this->input->post('password');
        $cpwd = $this->input->post('confirmpassword');
        $email = $this->input->post('email');

        if ($pwd == $cpwd) {
            /* $data = $this->Authentication->checkmobile($otp);
              if (!$data) {
              $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Access Denied! You have recently changed your password.</div>');
              return redirect('auth/unauthaccess');
              } */
            if ($this->Authentication->changepassword($pwd, $id)) {
                $newotp = $this->generateRandomStringOTP();
                $str = $this->Authentication->resendotp($otp, $newotp);
                $body = '<br>Your password has been changed successfully.<br>';
                $body .= '<a href="' . base_url('auth/') . '">Click here </a>to login.<br>';
                Send_Mail($email, 'canopus.testing@gmail.com', $body, "Password Changed");
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Success! Password change successfully. Please login.</div>');
                return redirect('auth/');
            }
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
              Error! Password does not match.</div>');
            $url = base_url('auth/resetpassword') . "/" . $otp;
            return redirect($url);
        }
    }

    public function logout() {
        
      $this->session->unset_userdata('user_id');
      $this->session->sess_destroy();
      return redirect('Auth');

    }

    public function paymentdetail() {

        $card_number = $this->input->post('card_number');
        $card_name = $this->input->post('card_name');
        $expiry_date = $this->input->post('expiry_date');
        $security_code = $this->input->post('security_code');

        $post = array('user_id' => 1, 'card_number' => $card_number, 'card_name' => $card_name, 'expiry_date' => $expiry_date, 'security_code' => $security_code);
        $wherearr = array('user_id' => 1);
        $row = $this->Authentication->select('payment_detail', $wherearr);
        if ($row->num_rows()) {
            $updatearr = array('user_id' => 1, 'card_number' => $card_number, 'card_name' => $card_name, 'expiry_date' => $expiry_date, 'security_code' => $security_code);
            $res = $this->Authentication->update('payment_detail', $wherearr, $updatearr);

            $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
			Success! Plan updated successfully.</div>');
            return redirect('admin/index');
        } else {
            $res = $this->Authentication->insert('payment_detail', $post);
            //var_dump($res);die;
            if ($res) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
				Success! Data inserted successfully.</div>');
                return redirect('admin/index');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button>
				Error! Error Occurred.</div>');

                return redirect('admin/index');
            }
        }
    }

    public function dashboard() {


        $this->load->view('admin/profileupdate');
    }

    public function getcountrylist() {
        $var[] = $this->Authentication->select_data('countries');
        echo json_encode($var[0]);
    }

    public function getsatatelist($id) {
        $var[] = $this->Authentication->select_data('states', array('country_id' => $id));
        echo json_encode($var[0]);
    }

    public function getcitylist($id) {
        $var[] = $this->Authentication->select_data('cities', array('state_id' => $id));
        echo json_encode($var[0]);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */