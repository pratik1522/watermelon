<div class="panel">
   <div class="col-md-12" >
        <br>
        <a href="<?php echo site_url('admin/createadmin'); ?>" class="btn btn-success" role="button" style="float: right">Create New User</a>

    </div>
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
            <li> <a href="javascript:void(0);">Admin List</a> </li>

        </ol>
    </div>
    <div class=panel-body>
        
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>Full Name 
                    <th>Email 
                    <th>Status
                    <th>Edit 
                    <th>Delete
                        
            <tbody>
                <?php foreach ($users as $key => $value) {
     
                    ?>
                    <tr>
                        <td><?php echo $value->firstname. ' '.$value->lastname; ?></td>
                        <td><?php echo $value->email; ?></td>
                        
                        <?php // print_r($value->user_status);die;?>
                        <td><?php if($value->user_status == 1){
                         ?>
                            <a href="<?php echo site_url('admin/updateadminstatus').'/'.$value->user_id.'/'.$value->user_status; ?>"><span class="label label-success">Active</span></a>
                            <?php }elseif($value->user_status == 2){ ?>
                                
                            <a href="<?php echo site_url('admin/updateadminstatus').'/'.$value->user_id.'/'.$value->user_status; ?>"><span class="label label-danger">Inactive</span></a>
                            <?php } ?></td>
                        
                        <td>
                            <a href="<?php echo site_url('admin/editadmin').'/'.$value->user_id; ?>" class=edit>Edit</a> 
                          </td>  
                        <td><a id="<?php echo $value->user_id ?>" href="<?php echo site_url('admin/deleteadmin').'/'.$value->user_id; ?>" >Delete</a>
                        </td> 
                        <?php } ?>
        </table>
    </div>
</div>
<script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>   
<script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>     
<script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
<script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script> 
<script>
$('#new').hide();
</script>