<div class="main-content">
    <?php 
  // var_dump($orders);
    ?>
    <div class="panel mb25">
        <div class="panel-heading border">Update User</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateadmin/').'/'.$this->uri->segment(3) ?>" method="post">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">First Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="firstname" required="" value="<?php echo $userdata[0]->firstname ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Last Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="lastname" required="" value="<?php echo $userdata[0]->lastname ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Email</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="email" required="" value="<?php echo $userdata[0]->email ?>"> 
                            </div>
                        </div>
                        
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Password</label> 
                            <div class=col-sm-10> 
                                <input type="password" class=form-control name="password" required=""  value="<?php echo $userdata[0]->password ?>"> 
                            </div>
                        </div>
                        
                       
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Update</button>
                             </div>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       