<div class="main-content">
    <?php 
  // var_dump($orders);
    ?>
    <div class="panel mb25">
        <div class="panel-heading border">Recipient Details</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Recipient Image</label> 
                            <div class=col-sm-10> 
                                <img src="<?php echo $recipientdetails[0]->received_user_image ?>" height="150" width="150" data-toggle="modal" data-target="#myModal"/> 
                            </div>
                        </div>                        
                    <div class=form-group>&nbsp;</div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Signature Image</label> 
                            <div class=col-sm-10> 
                                <img src="<?php echo $recipientdetails[0]->signature_image ?>" height="150" width="150" data-toggle="modal" data-target="#signimage"/> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Signature Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" value="<?php echo $recipientdetails[0]->signature_name ?>"> 
                            </div>
                        </div>
                        
   <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content" style="text-align:center;">
        <div class="modal-header"  style="background: rgba(0, 0, 0, 0.84);border:none;text-align:center;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
        </div>
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.84);">
          <p><img src="<?php echo $recipientdetails[0]->received_user_image ?>" width="200" style="border: 5px solid #fff;"></p>
        </div>        
      </div>      
    </div>
  </div>
        <!----------------model end------------------->   
         <!-- Modal -->
  <div class="modal fade" id="signimage" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content" style="text-align:center;">
        <div class="modal-header"  style="background: rgba(0, 0, 0, 0.84);border:none;text-align:center;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
        </div>
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.84);">
          <p><img src="<?php echo $recipientdetails[0]->signature_image ?> ?>" width="200" style="border: 5px solid #fff;"></p>
        </div>        
      </div>      
    </div>
  </div>
        <!----------------second model end------------------->   

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       