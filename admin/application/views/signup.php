<!DOCTYPE html> 
<html class=no-js>
    <!-- Mirrored from urban.nyasha.me/html/extras-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Jun 2016 09:30:25 GMT -->
    <head>
        <meta charset=utf-8>
        <title>urban admin ui kit html</title>
        <meta name=description content="">
        <meta name=viewport content="width=device-width">
        <script type="text/javascript">
            //<![CDATA[
            try{if (!window.CloudFlare) {var CloudFlare = [{verbose:0, p:0, byc:0, owlid:"cf", bag2:1, mirage2:0, oracle:0, paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"}, atok:"1095f4e946c96d943d91f95c7f6500cc", petok:"3b016e720c8fd7f2255b292adeafea969a2abfdb-1465378107-1800", zone:"nyasha.me", rocket:"0", apps:{"ga_key":{"ua":"UA-50530436-1", "ga_bs":"2"}}, sha2test:0}]; !function(a, b){a = document.createElement("script"), b = document.getElementsByTagName("script")[0], a.async = !0, a.src = "../../ajax.cloudflare.com/cdn-cgi/nexp/dok3v%3de982913d31/cloudflare.min.js", b.parentNode.insertBefore(a, b)}()}} catch (e){};
            //]]>
        </script>
        <link rel="shortcut icon" href=http://urban.nyasha.me/favicon.ec0f3a1b.ico>
        <link rel=stylesheet href=<?php echo base_url('styles/app.min.df5e9cc9.css'); ?>>
    <body>
        <div class="app layout-fixed-header bg-white usersession">
            <div class=full-height>
                <div class=center-wrapper>
                    <div class=center-content>
                        <div class="row no-margin">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <form role=form action='<?php echo site_url('auth/register'); ?>' class=form-layout method="post">
                                    <div class="text-center mb15"> 
                                       <img src="https://www.brandsoftheworld.com/sites/default/files/styles/logo-thumbnail/public/112011/citycourier.jpg?itok=JNok4pJI" alt> 
                                        <!--<img src="<?= base_url('images/logo-dark.5ba260bb.png'); ?>" alt>--> 
                                    </div>
                                    <p class="text-center mb30">Create your account.</p>
                                    <div class=form-inputs> 
                                        <input class="form-control input-lg" placeholder="First name" name="firstname" required="">
                                        <input class="form-control input-lg" placeholder="Last name" name="lastname" required=""> 
                                        <input class="form-control input-lg" placeholder="Email address" name="email" required="">
                                        <input class="form-control input-lg" placeholder="Contact number" name="phone" required=""> 
                                        <input type=password class="form-control input-lg" placeholder=Password name="password" required=""> 
                                    </div>
                                    <button class="btn btn-success btn-block btn-lg mb15" type=submit>Create Account</button> 
                                    <p class=text-left>By clicking signin up, you agree to our <a href=javascript:;>Terms of services &amp; Policies</a>.</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
