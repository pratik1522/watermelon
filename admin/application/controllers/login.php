<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Generalmodel');
    }

    public function index() {

        if ($this->session->userdata('logged_in') != false) {
            redirect('email/index');
        }
        $this->load->view('admin/viewcoupon');
    }

    public function login() {

        
        if ($this->session->userdata('logged_in') != false) {
            redirect('email/index');
        }
        $this->load->view('login');
    }

    public function signup() {

        if ($this->session->userdata('logged_in') != false) {
            redirect('email/index');
        }

        $this->load->view('Signup_view');
    }

    public function signup_Code() {
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $insert_id = $this->Generalmodel->signup($email, $pass);
        echo $insert_id;
    }

    public function login_Code() {
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        $insert_id = $this->Generalmodel->login($email, $pass);
        echo $insert_id;
    }

    public function logout() {
        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();
        $this->session->set_userdata(array('logged_in' => false));
        redirect('login/login');
    }

}
