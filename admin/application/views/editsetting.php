<div class=main-content style="padding-top: 10px">
    <?php 
//  echo '<pre>';
//        print_r($data);
//        die;
    ?>

    <div class="panel mb25">
        <div class="panel-heading border"><h4 style="font-weight:bold;">Minimum Charges</h4></div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateminimum/') ?>" method="post">
                 <div id="editable">       
                  <div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Vehicles<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

                            <div class=col-md-6>  
                              <button type="button" id="hide" class="btn btn-success pull-right" >Edit</button>
                            </div>             
                        </div>
                        <div class=form-group>
                            <label class="col-sm-1 control-label">MotorBike</label> 
                            <div class=col-sm-2> 
                                <input type="text" id="moto" class=form-control name="motorbike" required="" value="<?php if(isset($m)){echo $m->motorbike;}?>" readonly> 
                            </div>
                            <label class="col-sm-1 control-label">Car</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="car" required="" value="<?php if(isset($m)){echo $m->car;}?>" readonly> 
                            </div>
                            <label class="col-sm-1 control-label">Van</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="van" required="" value="<?php if(isset($m)){echo $m->van;}?>" readonly> 
                            </div>
                            <label class="col-sm-1 control-label">Truck</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="truck" required="" value="<?php if(isset($m)){echo $m->truck;}?>" readonly> 
                            </div>                            
                        </div>                       
                  </div>
                        <div id="submitable" style="display:none">
						<div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Vehicles<span style="font-size:13px;"> (price in $ per/km)</span></h4></label>

                            <div class=col-md-6>  
                              <button type="submit" class="btn btn-success pull-right" >Save</button>
                            </div>             
                        </div>
                        <div class=form-group>
                            <label class="col-sm-1 control-label">MotorBike</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="motorbike" required="" value="<?php if(isset($m)){echo $m->motorbike;}?>"> 
                            </div>
                            <label class="col-sm-1 control-label">Car</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="car" required="" value="<?php if(isset($m)){echo $m->car;}?>"> 
                            </div>
                            <label class="col-sm-1 control-label">Van</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="van" required="" value="<?php if(isset($m)){echo $m->van;}?>"> 
                            </div>
                            <label class="col-sm-1 control-label">Truck</label> 
                            <div class=col-sm-2> 
                                <input type="text" class=form-control name="truck" required="" value="<?php if(isset($m)){echo $m->truck;}?>"> 
                                <input type="hidden" class=form-control name="minimum_id" required="" value="<?php if(isset($m)){echo $m->id;}?>"> 
                            </div>                            
                        </div>                        
                    </div>
                        
                    <!--    <div class=form-group>
                            <label class="col-sm-2 control-label">Car</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="width" required="" value="<?php echo $data[0]->width ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Van</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="weight" required="" value="<?php echo $data[0]->weight ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Truck</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="weight" required="" value="<?php echo $data[0]->weight ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Save</button>
                             </div>
                        </div>-->
                        

                    </form>

                </div>
            </div>
        </div>
		
		<!--------Purchase Delivery-------------->
		<!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatepdmin/') ?>" method="post">
                 <div id="p_d_min_edit">       
                  <div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Purchase Delivery Minimum Charge<span style="font-size:13px;"></span></h4>         </label>

                            <div class=col-md-6>  
                              <button type="button" id="p_d_min" class="btn btn-success pull-right" >Edit</button>
                            </div>             
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Minimum Charges</label> 
                            <div class=col-sm-10> 
                                <input type="text" id="moto" class=form-control name="p_d_min_charge" required="" value="<?php //if(isset($setting)) { echo "$setting->p_d_min_charge";}?>" readonly> 
                            </div>                                                        
                        </div>                       
                  </div>
                    <div id="p_d_min_save" style="display:none">
						<div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Purchase Delivery Minimum Charge<span style="font-size:13px;"> </span></h4></label>

                            <div class=col-md-6>  
                              <button type="submit" class="btn btn-success pull-right" >Save</button>
                            </div>             
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Minimum Charges</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="p_d_min_charge" required="" value="<?php //if(isset($setting)) { echo "$setting->p_d_min_charge";}?>" > 
                            </div>
                            <input type="hidden" class=form-control name="p_d_min_id" required="" value="<?php //if(isset($setting)) { echo "$setting->id";}?>">
                        </div>                        
                    </div>                        
                    </form>
                </div>
            </div>
        </div>-->
<!--------------------End Purchase Delivery------------->
		
<!--standard parcel-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateparcel/') ?>" method="post">
                    <div id="parceledit">
						<div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Standard Parcel -<span style="font-size:13px;"> <?php if(isset($parcel)) { echo "$parcel->height";}?>x<?php if(isset($parcel)) { echo "$parcel->width";}?>x<?php if(isset($parcel)) { echo "$parcel->length";}?> cm per <?php if(isset($parcel)) { echo "$parcel->weight";}?> kg Cost - $<?php if(isset($parcel)) { echo "$parcel->cost";}?></span></h4></label>

                            <div class=col-md-6>  
                              <button type="button" id="parcel" class="btn btn-success pull-right" >Edit</button>
                            </div>             
                        </div>
                        <div class=form-group>                            
                            <label class="col-sm-1 control-label">Height (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="height" required="" value="<?php if(isset($parcel)) { echo "$parcel->height";}?>" readonly> 
                            </div>
                            <label class="col-sm-1 control-label">Width (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="width" required="" value="<?php if(isset($parcel)) { echo "$parcel->width";}?>" readonly> 
                            </div>
                            <label class="col-sm-1 control-label">Length (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="length" required="" value="<?php if(isset($parcel)) { echo "$parcel->length";}?>" readonly> 
                            </div>                            
                        </div>                        
                        <div class=form-group>
                         <label class="col-sm-2 control-label" style="margin-left:-40px;">Weight (/kg)</label> 
                            <div class=col-sm-4> 
                                <input type="text" class=form-control name="weight" required="" value="<?php if(isset($parcel)) { echo "$parcel->weight";}?>" readonly> 
                            </div>   
                         <label class="col-sm-2 control-label" style="margin-left:-22px;">Cost ($)</label> 
                            <div class=col-sm-4> 
                                <input type="text" class=form-control name="cost" required="" value="<?php if(isset($parcel)) { echo "$parcel->cost";}?>" readonly> 
                            </div>
                                                        
                        </div>
                    </div>
					<div id="parcelsave" style="display:none;">
						<div class=form-group>
                            <label class="col-md-6 control-label"><h4 style="float:left;">Standard Parcel -<span style="font-size:13px;"> <?php if(isset($parcel)) { echo "$parcel->height";}?>x<?php if(isset($parcel)) { echo "$parcel->width";}?>x<?php if(isset($parcel)) { echo "$parcel->length";}?> cm per <?php if(isset($parcel)) { echo "$parcel->weight";}?> kg Cost - $<?php if(isset($parcel)) { echo "$parcel->cost";}?></span></h4></label>

                            <div class=col-md-6>  
                              <button type="submit" class="btn btn-success pull-right" >Save</button>
                            </div>             
                        </div>
                        <div class=form-group>                            
                            <label class="col-sm-1 control-label">Height (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="height" required="" value="<?php if(isset($parcel)) { echo "$parcel->height";}?>"> 
                            </div>
                            <label class="col-sm-1 control-label">Width (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="width" required="" value="<?php if(isset($parcel)) { echo "$parcel->width";}?>"> 
                            </div>
                            <label class="col-sm-1 control-label">Length (/cm)</label> 
                            <div class=col-sm-3> 
                                <input type="text" class=form-control name="length" required="" value="<?php if(isset($parcel)) { echo "$parcel->length";}?>"> 
                            </div>                            
                        </div>                        
                        <div class=form-group>
                         <label class="col-sm-2 control-label" style="margin-left:-22px;">Weight (/kg)</label> 
                            <div class=col-sm-4> 
                                <input type="text" class=form-control name="weight" required="" value="<?php if(isset($parcel)) { echo "$parcel->weight";}?>"> 
                            </div>   
                         <label class="col-sm-2 control-label" style="margin-left:-22px;">Cost ($)</label> 
                            <div class=col-sm-4> 
                                <input type="text" class=form-control name="cost" required="" value="<?php if(isset($parcel)) { echo "$parcel->cost";}?>"> 
                                <input type="hidden" class=form-control name="parcel_id" required="" value="<?php if(isset($parcel)) { echo "$parcel->id";}?>"> 
                            </div>
                                                        
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>-->
<!--end standard parcel-->

<!--standard parcel Size-->
    <!--    <div class="panel-heading border"><h4>Standard Parcel Size</h4></div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatemparcelsizesetting/') ?>" method="post">
						<div id="mpsizeedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">MotorBike<span style="font-size:13px;"> </span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="motor_psize" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="m_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_height";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_length";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_width";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_weight";}?>" readonly> 
								</div>
							</div>                        
						</div>
						<div id="mpsizesave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">MotorBike<span style="font-size:13px;"> </span></h4>         </label>

								<div class=col-md-6>  
								  <button type="save" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="m_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_height";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_length";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_width";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="m_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->m_p_weight";}?>"> 
									<input type="hidden" class=form-control name="m_p_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>								                     
							</div>                        
						</div>
                    </form>

                </div>
            </div>
        </div>-->
<!--End Motorbike Size-->
<!--car Size-->
    <!--    <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatecparcelsizesetting/') ?>" method="post">
						<div id="cpsizeedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Car<span style="font-size:13px;"> </span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="car_psize" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="c_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_height";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_length";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_width";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_weight";}?>" readonly> 
								</div>
							</div>                        
						</div>
						<div id="cpsizesave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Car<span style="font-size:13px;"> </span></h4> </label>

								<div class=col-md-6>  
								  <button type="save" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="c_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_height";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_length";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_width";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="c_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->c_p_weight";}?>"> 
									<input type="hidden" class=form-control name="c_p_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>								                     
							</div>                        
						</div>
                    </form>
                </div>
            </div>
        </div>-->
<!--end car Size-->
<!--Van Size-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatevparcelsizesetting/') ?>" method="post">
						<div id="vpsizeedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Van<span style="font-size:13px;"> </span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="van_psize" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="v_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_height";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_length";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_width";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_weight";}?>" readonly> 
								</div>
							</div>                        
						</div>
						<div id="vpsizesave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Van<span style="font-size:13px;"> </span></h4> </label>

								<div class=col-md-6>  
								  <button type="save" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="v_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_height";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_length";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_width";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="v_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->v_p_weight";}?>"> 
									<input type="hidden" class=form-control name="v_p_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>								                     
							</div>                        
						</div>
                    </form>
                </div>
            </div>
        </div>-->
<!--end van Size-->
<!--Truck Size-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatetparcelsizesetting/') ?>" method="post">
						<div id="tpsizeedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Truck<span style="font-size:13px;"> </span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="truck_psize" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="t_p_height" required="" value="<?php if(isset($setting)) { echo "$setting->t_p_height";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_length" required="" value="<?php if(isset($setting)) { echo "$setting->t_p_length";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_width" required="" value="<?php if(isset($setting)) { echo "$setting->t_p_width";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_weight" required="" value="<?php if(isset($setting)) { echo "$setting->t_p_weight";}?>" readonly> 
								</div>
							</div>                        
						</div>
						<div id="tpsizesave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Truck<span style="font-size:13px;"> </span></h4> </label>

								<div class=col-md-6>  
								  <button type="save" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">Height</label> 
								<div class=col-sm-2> 
									<input type="text" id="moto" class=form-control name="t_p_height" required="" value="<?php //if(isset($setting)) { echo "$setting->t_p_height";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Length</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_length" required="" value="<?php //if(isset($setting)) { echo "$setting->t_p_length";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Width</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_width" required="" value="<?php //if(isset($setting)) { echo "$setting->t_p_width";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Weight</label> 
								<div class=col-sm-2> 
									<input type="text" class=form-control name="t_p_weight" required="" value="<?php //if(isset($setting)) { echo "$setting->t_p_weight";}?>"> 
									<input type="hidden" class=form-control name="t_p_id" required="" value="<?php //if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>								                     
							</div>                        
						</div>
                    </form>
                </div>
            </div>
        </div>-->
<!--end Truck Size-->
<!--end standard parcel Size-->
    </div>
<!--vechiles-->

<div class="panel mb25">
<!--Motorbike-->
<!--<div class="panel-heading border"><h4 style="font-weight:bold;">Save Settings</h4></div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatemotorsetting/') ?>" method="post">
						<div id="motoredit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">MotorBike<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="motor" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="m_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->m_2hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="m_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->m_4hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="m_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->m_sameday";}?>" readonly> 
								</div>								                            
							</div>                        
						</div>
						<div id="motorsave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">MotorBike<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="save" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="m_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->m_2hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="m_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->m_4hour";}?>" > 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="m_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->m_sameday";}?>"> 
									<input type="hidden" class=form-control name="m_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>                      
							</div>                        
						</div>
                    </form>

                </div>
            </div>
        </div>-->
<!--End Motorbike-->
<!--car-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatecarsetting/') ?>" method="post">
						<div id="caredit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Car<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="car" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="c_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->c_2hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="c_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->c_4hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="c_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->c_sameday";}?>" readonly> 
								</div>								                            
							</div>                       
						</div>
						<div id="carsave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Car<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="submit" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="c_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->c_2hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="c_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->c_4hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="c_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->c_sameday";}?>">
									<input type="hidden" class=form-control name="c_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>
								                            
							</div>                       
						</div>
                    </form>
                </div>
            </div>
        </div>-->
<!--end car-->
<!--Van-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatevansetting/') ?>" method="post">
						<div id="vanedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Van<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="van" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="v_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->v_2hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="v_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->v_4hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="v_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->v_sameday";}?>" readonly> 
								</div>								                            
							</div>                        
						</div>
						<div id="vansave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Van<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="submit" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="v_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->v_2hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="v_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->v_4hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="v_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->v_sameday";}?>"> 
									<input type="hidden" class=form-control name="v_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>"> 
								</div>								                            
							</div>                        
						</div>
                    </form>

                </div>
            </div>
        </div>-->
<!--end van-->
<!--Truck-->
        <!--<div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatetrucksetting/') ?>" method="post">
						<div id="truckedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Truck<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="truck" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="t_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->t_2hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="t_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->t_4hour";}?>" readonly> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="t_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->t_sameday";}?>" readonly> 
								</div>								                            
							</div>                        
						</div>
						<div id="trucksave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">Truck<span style="font-size:13px;"> (price in $ per/km)</span></h4>         </label>

								<div class=col-md-6>  
								  <button type="submit"  class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-1 control-label">2 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" id="moto" class=form-control name="t_2hour" required="" value="<?php if(isset($setting)) { echo "$setting->t_2hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="t_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->t_4hour";}?>"> 
								</div>
								<label class="col-sm-1 control-label">Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="t_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->t_sameday";}?>"> 
			                                                 <input type="hidden" class=form-control name="t_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>">						
								</div>                      
							</div>                        
						</div>
                    </form>

                </div>
            </div>
        </div>-->
<!--end Truck-->
<!--End Time 4 Hours and Same Day-->
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateendtimesetting/') ?>" method="post">
						<div id="endtimeedit">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">End TIme<span style="font-size:13px;"></span></h4>         </label>

								<div class=col-md-6>  
								  <button type="button" id="endtime" class="btn btn-success pull-right" >Edit</button>
								</div>             
							</div>
							<div class=form-group>								
								<label class="col-sm-3 control-label">End Time 4 Hours</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="endtime_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->endtime_4hour";}?>" readonly> 
								</div>
								<label class="col-sm-3 control-label">End Time Same Day</label> 
								<div class=col-sm-3> 
									<input type="text" class=form-control name="endtime_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->endtime_sameday";}?>" readonly> 
								</div>								                            
							</div>                        
						</div>
						<div id="endtimesave" style="display:none;">       
							<div class=form-group>
								<label class="col-md-6 control-label"><h4 style="float:left;">End Time<span style="font-size:13px;"></span></h4>         </label>

								<div class=col-md-6>  
								  <button type="submit"  class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>								
								<label class="col-sm-3 control-label">End Time 4 Hours</label> 
								<div class=col-sm-3> 
									<select class=form-control name="endtime_4hour" required="" value="<?php if(isset($setting)) { echo "$setting->endtime_4hour";}?>"> 
									<option value="01:00">01:00</option>
									<option value="02:00">02:00</option>
									<option value="03:00">03:00</option>
									<option value="04:00">04:00</option>
									<option value="05:00">05:00</option>
									<option value="06:00">06:00</option>
									<option value="07:00">07:00</option>
									<option value="08:00">08:00</option>
									<option value="09:00">09:00</option>
									<option value="10:00">10:00</option>
									<option value="11:00">11:00</option>
									<option value="12:00">12:00</option>
									<option value="13:00">13:00</option>
									<option value="14:00">14:00</option>
									<option value="15:00">15:00</option>
									<option value="16:00">16:00</option>
									<option value="17:00">17:00</option>
									<option value="18:00">18:00</option>
									<option value="19:00">19:00</option>
									<option value="20:00">20:00</option>
									<option value="21:00">21:00</option>
									<option value="22:00">22:00</option>
									<option value="23:00">23:00</option>
									</select>
								</div>
								<label class="col-sm-3 control-label">End Time Same Day</label> 
								<div class=col-sm-3> 
									<select class=form-control name="endtime_sameday" required="" value="<?php if(isset($setting)) { echo "$setting->endtime_sameday";}?>"> 
									<option value="01:00">01:00</option>
									<option value="02:00">02:00</option>
									<option value="03:00">03:00</option>
									<option value="04:00">04:00</option>
									<option value="05:00">05:00</option>
									<option value="06:00">06:00</option>
									<option value="07:00">07:00</option>
									<option value="08:00">08:00</option>
									<option value="09:00">09:00</option>
									<option value="10:00">10:00</option>
									<option value="11:00">11:00</option>
									<option value="12:00">12:00</option>
									<option value="13:00">13:00</option>
									<option value="14:00">14:00</option>
									<option value="15:00">15:00</option>
									<option value="16:00">16:00</option>
									<option value="17:00">17:00</option>
									<option value="18:00">18:00</option>
									<option value="19:00">19:00</option>
									<option value="20:00">20:00</option>
									<option value="21:00">21:00</option>
									<option value="22:00">22:00</option>
									<option value="23:00">23:00</option>
									
									</select>
			                        <input type="hidden" class=form-control name="endtime_id" required="" value="<?php if(isset($setting)) { echo "$setting->id";}?>">						
								</div>                      
							</div>                        
						</div>
                    </form>

                </div>
            </div>
        </div>
<!--end time 4hours and sameday-->
    </div>

<!--vechiles-->	
    <!--<div class="panel mb25" style="display:none;">
        <div class="panel-heading border">Save Settings</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatesetting/') ?>" method="post">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Height (cm)</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="height" required="" value="<?php echo $data[0]->height ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Width (cm)</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="width" required="" value="<?php echo $data[0]->width ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Weight (gm)</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="weight" required="" value="<?php echo $data[0]->weight ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Length (cm)</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="length" required="" value="<?php echo $data[0]->length ?>"> 
                            </div>
                        </div>
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Distance (miles)</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="distance" required=""  value="<?php echo $data[0]->distance ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Bike</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="bike" required=""  value="<?php echo $data[0]->bike ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Car</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="car" required=""  value="<?php echo $data[0]->car ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Truck</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="truck" required=""  value="<?php echo $data[0]->truck ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Van</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="van" required=""  value="<?php echo $data[0]->van ?>"> 
                                
                            </div>
                        </div>
                    
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Save</button>
                             </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>-->
	
	<!--------driver percentage------------------>
	<div class="panel mb25">
	<div class="panel-heading border"><h4 style="font-weight:bold;">Admin Percentage</h4></div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updatedriversetting/') ?>" method="post">
						<div id="driveredit">       
							<div class=form-group>
									<label class="col-md-6 control-label"><h4 style="float:left;"><span style="font-size:13px;"></span></h4></label>
								<div class=col-md-6>  
									<button type="button" id="driver_per" class="btn btn-success pull-right" >Edit</button>
							</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-2 control-label">Percentage (price in %)</label> 
								<div class=col-sm-10> 
									<input type="text" id="moto" class=form-control name="driver_percentage" required="" value="<?php if(isset($setting)){echo $setting->driver_percentage;}?>" readonly> 
								</div>                      
							</div>                       
						</div>
                        <div id="driversave" style="display:none">
							<div class=form-group>
<label class="col-md-6 control-label"><h4 style="float:left;"><span style="font-size:13px;"></span></h4></label>
								<div class=col-md-6>  
								  <button type="submit" class="btn btn-success pull-right" >Save</button>
								</div>             
							</div>
							<div class=form-group>
								<label class="col-sm-2 control-label">Percentage (price in %)</label> 
								<div class=col-sm-10> 
									<input type="text" class=form-control name="driver_percentage" required="" value="<?php if(isset($setting)){echo $setting->driver_percentage;}?>"> 
									<input type="hidden" class=form-control name="percentage_id" required="" value="<?php if(isset($setting)) { echo $setting->id;}?>">
								</div>                      
							</div>                        
						</div>
                    </form>
                </div>
            </div>
        </div>
	</div>
	<!-------End driver percentage---------------->
	
	
	
</div>
</div>

</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#hide").click(function(){
        $("#editable").hide(); 
        $("#submitable").show();
    });
    $("#parcel").click(function(){
        $("#parceledit").hide();
        $("#parcelsave").show();
    });
	
	$("#motor").click(function(){
        $("#motoredit").hide();
        $("#motorsave").show();
    });
	$("#car").click(function(){
        $("#caredit").hide();
        $("#carsave").show();
    });
	$("#van").click(function(){
        $("#vanedit").hide();
        $("#vansave").show();
    });
	$("#truck").click(function(){
        $("#truckedit").hide();
        $("#trucksave").show();
    });
	$("#driver_per").click(function(){
        $("#driveredit").hide();
        $("#driversave").show();
    });
	$("#endtime").click(function(){
        $("#endtimeedit").hide();
        $("#endtimesave").show();
    });
	$("#motor_psize").click(function(){
        $("#mpsizeedit").hide();
        $("#mpsizesave").show();
    });
	$("#car_psize").click(function(){
        $("#cpsizeedit").hide();
        $("#cpsizesave").show();
    });
	$("#van_psize").click(function(){
        $("#vpsizeedit").hide();
        $("#vpsizesave").show();
    });
	$("#truck_psize").click(function(){
        $("#tpsizeedit").hide();
        $("#tpsizesave").show();
    });
	$("#p_d_min").click(function(){
        $("#p_d_min_edit").hide();
        $("#p_d_min_save").show();
    });
});
</script>

<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">    