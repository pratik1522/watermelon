<div class="main-content">
    <div class="panel mb25">
        <div class="panel-heading border">Create New Product</div>
        <div class="panel-body">
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/updateproduct/').'/'.$this->uri->segment(3) ?>" method="post" enctype="multipart/form-data">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Title</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="title" required="" value="<?php echo $product[0]->title ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Image</label> 
                            <div class=col-sm-10> 
                                <input type="file" name="image" > 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Price</label> 
                            <div class=col-sm-10> 
                                <input type="number" step="any"class=form-control name="price" required=""  value="<?php echo $product[0]->price ?>"/>
                            </div>
                        </div>

                        <div class=form-group>
                            <label class="col-sm-2 control-label">Colour</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="colour" required="" value="<?php echo $product[0]->colour ?>">
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Length</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="length" required="" value="<?php echo $product[0]->length ?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Height</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="height" required="" value="<?php echo $product[0]->height ?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Width</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="width" required="" value="<?php echo $product[0]->width?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Weight</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="weight" required="" value="<?php echo $product[0]->weight?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Seller Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="seller" required="" value="<?php echo $product[0]->seller_name?>" >
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Descriptions</label> 
                            <div class=col-sm-10> 
                                <textarea class=form-control name="descriptions" required="" ><?php echo $product[0]->description ?> </textarea>
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Update Product</button>
                             </div>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       