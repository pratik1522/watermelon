 <?php $api_url = $this->config->item('api_url');   ?>
<script>
$('#new').hide();
</script>
<div class="panel">
   <div class="col-md-12" >
        <br>
   
    </div>
    <div class="panel-heading border">
        <ol class="breadcrumb mb0 no-padding">
             <li>Driver List</li>
            <li>
            
             <!-----file export-->

                               <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                            <li class="divider"></li>                                                
                                        <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"><img src='<?php echo base_url('assest/img/icons/xls.png');?>' width="24"/> XLS</a></li>
                                           <!--  <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#example').tableExport({type:'png',escape:'false'});"><img src='<?php //echo base_url('assest/img/icons/png.png');?>' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#example').tableExport({type:'pdf',escape:'false'});"><img src='<?php //echo base_url('assest/img/icons/pdf.png');?>' width="24"/> PDF</a></li> -->
                                        </ul>
                                    </div>

                <!--end-->
            </li>

        </ol>
           
    </div>
  

<style>
.scroll_tabb .table-responsive{overflow-x: scroll;
}
</style>

 
    <div class="panel-body">
    <div class="scroll_tabb">
        <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered display nowrap" id="example">
            <thead>
                <tr>
                    <th>Driver Id</th>
                    <th>Full Name </th>
                    <th>Email </th>
                    <th>Image</th>
                    <th>DOB</th>
                    <th>Contact No </th>
                    <th>Address </th>
                    <th>Status</th>
                    <th>User Type</th>
                    <th>Edit </th>
                     <th>Delete</th> 
                    </tr>
                    </thead>    
            <tbody>
                <?php foreach ($users as $key => $value) {
     
                    ?>
                    <tr>
                    	<td><?php echo $value->user_id; ?></td>
                        <td><?php echo $value->firstname. ' '.$value->lastname; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><img src="<?php echo base_url(); ?>image/<?php echo $value->profile_image; ?>" width="50" data-toggle="modal" data-target="#myModal<?php echo $value->user_id; ?>"> </td>
                        <td><?php echo $value->dob; ?></td>
                        <td><?php echo $value->phone; ?></td>
                        <td><?php echo $value->unit_no; ?><?php echo $value->house_no; ?>,<?php echo $value->street_name; ?><?php echo $value->street_suf; ?>-<?php echo $value->country_name; ?></td>
                        <td><?php if ($value->user_status == 1) { ?>
                            <a href="<?php echo site_url('admin/updateuserstatus').'/'.$value->user_id.'/'.$value->user_status; ?>"><span class="label label-success">Active</span></a>
                            <?php } else if ($value->user_status == 2) { ?>                                
                            <a href="<?php echo site_url('admin/updateuserstatus').'/'.$value->user_id.'/'.$value->user_status; ?>"><span class="label label-danger">Inactive</span></a>
                            <?php } ?></td>
                        <td>
                            <?php if ($value->user_type == 1) { ?>
                            Customer
                            <?php }else{ ?>
                            Driver
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($value->user_type == 1) { ?>
                            <a href="<?php echo site_url('admin/edituser').'/'.$value->user_id; ?>" class=edit>Edit</a> 
                            <?php }else{ ?>
                            <a href="<?php echo site_url('admin/editdriver').'/'.$value->user_id; ?>" class=edit>Edit</a> 
                            <?php } ?></td>
                     <td><a id="<?php echo $value->user_id ?>" href="<?php echo site_url('admin/deletedriver').'/'.$value->user_id; ?>" >Delete</a> </td> 
                        
                        <!-- Modal -->
  <div class="modal fade" id="myModal<?php echo $value->user_id; ?>" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content" style="text-align:center;">
        <div class="modal-header"  style="background: rgba(0, 0, 0, 0.84);border:none;text-align:center;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
        </div>
        <div class="modal-body" style="background: rgba(0, 0, 0, 0.84);">
          <p><img src="<?php echo $api_url;?>upload/<?php echo $value->profile_image; ?>" width="200" style="border: 5px solid #fff;"></p>
        </div>        
      </div>      
    </div>
  </div>
        <!----------------model end------------------>                
                        <?php } ?>
                        </tr>
        </table>
        </div>
    </div>
</div>
 <script src=<?= base_url('scripts/app.min.4fc8dd6e.js'); ?>></script>      
<!-- <script src=<?= base_url('vendor/datatables/media/js/jquery.dataTables.js'); ?>></script>    
<script src="https://cdn.datatables.net/buttons/1.2.3/js/dataTables.buttons.min.js"></script>  -->
<!-- <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
<script src=<?= base_url('scripts/extentions/bootstrap-datatables.8df42543.js'); ?>></script> 
<script src=<?= base_url('scripts/pages/table-edit.adb541fe.js'); ?>></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>


  <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/datatables/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/tableExport.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jquery.base64.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/html2canvas.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/plugins/tableexport/jspdf/libs/sprintf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/jspdf.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assest/js/plugins/tableexport/jspdf/libs/base64.js');?>"></script>  
 
<script>
$('#new').hide();

$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>