<div class="main-content" style="padding-top: 35px;">
    <?php echo $this->session->flashdata('msg'); ?>
    <?php 
  // var_dump($orders);
    ?>
    <div class="panel mb25">
        <div class="panel-heading border">Update User</div>
        <div class=panel-body>
            <div class="row no-margin">
                <div class=col-lg-12>
                    <form class="form-horizontal bordered-group" role=form action="<?= site_url('admin/edituser_code/').'/'.$this->uri->segment(3) ?>" method="post">
                        <div class=form-group>
                            <label class="col-sm-2 control-label">First Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="firstname" pattern="[a-zA-Z]{2,20}" required="" value="<?php echo $userdata[0]->firstname ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Last Name</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="lastname" pattern="[a-zA-Z]{2,20}" required="" value="<?php echo $userdata[0]->lastname ?>"> 
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Email</label> 
                            <div class=col-sm-10> 
                                <input type="email" class=form-control name="email" required="" value="<?php echo $userdata[0]->email ?>"> 
                            </div>
                        </div>
                        
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Contact No</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="phone"  required="" value="<?php echo $userdata[0]->phone ?>"> <!--pattern="[0-9]{10}"-->
                            </div>
                        </div>
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Landline No.</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="land_line_no" value="<?php echo $userdata[0]->land_line_no ?>"> 
                                
                            </div>
                        </div>
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Unit No</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="unit_no"   value="<?php echo $userdata[0]->unit_no ?>"> 
                                
                            </div>
                        </div>
                        
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">House No.</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="house_no"  value="<?php echo $userdata[0]->house_no ?>"> 
                                
                            </div>
                        </div>
                        
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Street Name</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="street_name" required=""  value="<?php echo $userdata[0]->street_name ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Street Suff.</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="street_suf" required=""  value="<?php echo $userdata[0]->street_suf ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Suburb</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="suburb" required=""  value="<?php echo $userdata[0]->suburb ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">State</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="state" required=""  value="<?php echo $userdata[0]->state ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Postcode</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="postcode" required=""  value="<?php echo $userdata[0]->postcode ?>"> 
                                
                            </div>
                        </div>
                        <div class=form-group>
                            <label class="col-sm-2 control-label">Country</label> 
                            <div class=col-sm-10>
                                <input type="text" class=form-control name="country_name" required=""  value="<?php echo $userdata[0]->country_name ?>"> 
                                
                            </div>
                        </div>
<!--                        <div class=form-group>
                            <label class="col-sm-2 control-label">Password</label> 
                            <div class=col-sm-10> 
                                <input type="text" class=form-control name="password" required="" value="<?php echo $userdata[0]->password ?>"> 
                            </div>
                        </div>-->
                    
                        
                        <div class=form-group>
                            <label class="col-sm-2 control-label"></label> 
                            <div class=col-sm-10> 
                                    <button type="submit" class="btn btn-success" >Update User</button>
                             </div>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
<script src=<?php echo base_url('scripts/app.min.4fc8dd6e.js'); ?>></script> <script type="text/javascript">
       